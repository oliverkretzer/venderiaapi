package de.finnilius.api.methods;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import de.finnilius.api.Main;
import de.finnilius.api.mysql.MySQLStats;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class API {

    public static void sendMessageWithDelay(Player player, String message, int seconds) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
            public void run() {
                player.sendMessage("" + message);
                player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1.0F, 1.0F);
            }
        }, seconds * 20);
    }

    public static void checkXP(Player player) {
        int currentXP = MySQLStats.getEXP(player.getUniqueId().toString());
        int currentLevel = MySQLStats.getLevel(player.getUniqueId().toString());
        if (currentXP == 500) {
            MySQLStats.setLevel(player.getUniqueId().toString(), 1);
            player.sendMessage("�8(�6Level�8) �aDu hast erfolgreich Level 1 erreicht!");
            sendTitle(player, "�6�lLevel " + (currentLevel + 1), "�fDu bist ein Level aufgestiegen!", 25, 75, 25);
            return;
        }

        double neededXP = 500 * 2.25 * currentLevel;
        if (currentXP >= neededXP) {
            player.sendMessage("�8(�6Level�8) �aDu hast erfolgreich Level " + (currentLevel + 1) + " erreicht!");
            sendTitle(player, "�6�lLevel " + (currentLevel + 1), "�fDu bist ein Level aufgestiegen!", 25, 75, 25);
            MySQLStats.setLevel(player.getUniqueId().toString(), MySQLStats.getLevel(player.getUniqueId().toString()) + 1);
            MySQLStats.setEXP(player.getUniqueId().toString(), (int) (MySQLStats.getEXP(player.getUniqueId().toString()) - 500 * 2.25 * currentLevel));
            return;
        }
    }

    public static void addXP(Player player, int amount) {
        MySQLStats.setEXP(player.getUniqueId().toString(), MySQLStats.getEXP(player.getUniqueId().toString()) + amount);

        int currentXP = MySQLStats.getEXP(player.getUniqueId().toString());
        int currentLevel = MySQLStats.getLevel(player.getUniqueId().toString());
        double neededXP = 500 * 2.25 * currentLevel;

        checkXP(player);
        sendActionbarMessage(player, "�a+ �f" + amount + " �fEXP �8(�7" + currentXP + "/" + (int) neededXP + "�8)");
    }

    public static void sendActionbarMessage(Player player, String message) {
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
    }

    //Sendet einen Title
    public static void sendTitle(Player player, String firstTitle, String secondTitle, int timeToSpawn, int timeShowTitle, int timeToDespawn) {
        player.sendTitle(firstTitle, secondTitle, timeToSpawn, timeShowTitle, timeToDespawn);
    }

    public static void sendTabTitle(Player player, String header, String footer) {
        if (header == null)
            header = "";
        header = ChatColor.translateAlternateColorCodes('&', header);
        if (footer == null)
            footer = "";
        footer = ChatColor.translateAlternateColorCodes('&', footer);
        Tablist tabTitleSendEvent = new Tablist(player, header, footer);
        Bukkit.getPluginManager().callEvent(tabTitleSendEvent);
        if (tabTitleSendEvent.isCancelled())
            return;
        header = header.replaceAll("%player%", player.getDisplayName());
        footer = footer.replaceAll("%player%", player.getDisplayName());
        try {
            Object tabHeader = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke(null, new Object[]{"{\"text\":\"" + header + "\"}"});
            Object tabFooter = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke(null, new Object[]{"{\"text\":\"" + footer + "\"}"});
            Constructor<?> titleConstructor = getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor(new Class[0]);
            Object packet = titleConstructor.newInstance(new Object[0]);
            try {
                Field aField = packet.getClass().getDeclaredField("a");
                aField.setAccessible(true);
                aField.set(packet, tabHeader);
                Field bField = packet.getClass().getDeclaredField("b");
                bField.setAccessible(true);
                bField.set(packet, tabFooter);
            } catch (Exception e) {
                Field aField = packet.getClass().getDeclaredField("header");
                aField.setAccessible(true);
                aField.set(packet, tabHeader);
                Field bField = packet.getClass().getDeclaredField("footer");
                bField.setAccessible(true);
                bField.set(packet, tabFooter);
            }
            sendPacket(player, packet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void sendPacket(Player player, Object packet) {
        try {
            Object handle = player.getClass().getMethod("getHandle", new Class[0]).invoke(player, new Object[0]);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", new Class[]{getNMSClass("Packet")}).invoke(playerConnection, new Object[]{packet});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Class<?> getNMSClass(String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            return Class.forName("net.minecraft.server." + version + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ItemStack getArrowLeft() {
        ItemStack back = API.getCustomSkull("http://textures.minecraft.net/texture/bd69e06e5dadfd84e5f3d1c21063f2553b2fa945ee1d4d7152fdc5425bc12a9");
        SkullMeta backMeta = (SkullMeta) back.getItemMeta();
        backMeta.setDisplayName("�6Zur�ck");
        backMeta.setLore(Arrays.asList(new String[]{"�6Klicke, �7um zur�ck zu kehren."}));
        back.setItemMeta(backMeta);
        return back;
    }

    public static ItemStack getArrowRight() {
        ItemStack back = API.getCustomSkull("http://textures.minecraft.net/texture/19bf3292e126a105b54eba713aa1b152d541a1d8938829c56364d178ed22bf");
        SkullMeta backMeta = (SkullMeta) back.getItemMeta();
        backMeta.setDisplayName("�6Weiter");
        backMeta.setLore(Arrays.asList(new String[]{"�6Klicke, �7um fortzufahren."}));
        back.setItemMeta(backMeta);
        return back;
    }

    public static enum Punishment {
        FARM1("Illegale Farm (10 Bl�cke)", 10),
        FARM2("Illegale Farm (25 Bl�cke)", 25),
        FARM3("Illegale Farm (50 Bl�cke)", 50),
        BUILD("Versto� gegen die Bauregeln", 69);

        private String name;
        private double amount;

        private Punishment(String name, double amount) {
            this.name = name;
            this.amount = amount;
        }

        public String getName() {
            return name;
        }

        public double getAmount() {
            return amount;
        }
    }

    //Formartiert das Geld in 1.870,00 �
    public static String formatMoney(double amount) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String formatted = formatter.format(amount);
        return formatted;
    }

    public static int getAmount(Player p, ItemStack item) {
        if (item == null)
            return 0;
        int amount = 0;
        for (int i = 0; i < 36; i++) {
            ItemStack itemstack = p.getInventory().getItem(i);
            if (itemstack == null || !itemstack.isSimilar(item))
                continue;
            amount += itemstack.getAmount();
        }
        return amount;
    }

    //Custom Kopf mit URL
    private static Field profileField;

    public static ItemStack getCustomSkull(String url) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) item.getItemMeta();

        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        byte[] data = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
        profile.getProperties().put("textures", new Property("textures", new String(data)));

        try {
            if (profileField == null) {
                profileField = meta.getClass().getDeclaredField("profile");
            }

            profileField.setAccessible(true);
            profileField.set(meta, profile);

            item.setItemMeta(meta);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return item;
    }

    //String zur Location
    public static String locationToString(Location loc, boolean yawpitch) {
        return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + (yawpitch ? ";" + loc.getYaw() + ";" + loc.getPitch() : "");
    }

    //Location zum String
    public static Location stringToLocation(String stringloc, boolean yawpitch) {
        String[] splitted = stringloc.split(";");
        Location loc = null;
        if (yawpitch) {
            loc = new Location(Bukkit.getWorld(splitted[0]), Double.valueOf(splitted[1]).doubleValue(), Double.valueOf(splitted[2]).doubleValue(),
                    Double.valueOf(splitted[3]).doubleValue(), Float.valueOf(splitted[4]).floatValue(), Float.valueOf(splitted[5]).floatValue());
        } else {
            loc = new Location(Bukkit.getWorld(splitted[0]), Double.valueOf(splitted[1]).doubleValue(), Double.valueOf(splitted[2]).doubleValue(), Double.valueOf(splitted[3]).doubleValue());
        }
        return loc;
    }

    //Pr�ft ob Items im Inventar verf�gbar sind
    public static int getAvailableItems(Inventory inv, ItemStack item) {
        int counted = 0;
        for (ItemStack content : inv.getContents()) {
            if ((content != null) && (content.getType() != Material.AIR) && (content.getType() == item.getType()) && (content.getDurability() == item.getDurability())) {
                counted += content.getAmount();
            }
        }
        return counted;
    }

    //Pr�ft ob jemand gen�gend Items hat
    public static boolean hasEnoughItems(Inventory inv, ItemStack item, int amount) {
        return getAvailableItems(inv, item) >= amount;
    }

    //Entfernt Items
    public static boolean removeItems(Inventory inv, ItemStack item, int amount) {
        if (!hasEnoughItems(inv, item, amount)) return false;
        int toRemove = amount;
        HashMap<Integer, ItemStack> slots = new HashMap();
        ItemStack slotItem;
        for (int slot = 0; slot < inv.getSize(); slot++) {
            slotItem = inv.getItem(slot);
            if ((slotItem != null) && (slotItem.getType() != Material.AIR) && (slotItem.getType() == item.getType()) && (slotItem.getDurability() == item.getDurability())) {
                slots.put(Integer.valueOf(slot), slotItem);
            }
        }

        for (Entry entrySlots : slots.entrySet()) {
            if (((ItemStack) entrySlots.getValue()).getAmount() <= toRemove) {
                inv.setItem(((Integer) entrySlots.getKey()).intValue(), new ItemStack(Material.AIR));
                toRemove -= ((ItemStack) entrySlots.getValue()).getAmount();
            } else {
                ItemStack invItem = inv.getItem(((Integer) entrySlots.getKey()).intValue());
                invItem.setAmount(invItem.getAmount() - toRemove);
                break;
            }
        }
        return true;
    }

    //F�gt ein Item hinzu
    public static void addItem(Player p, ItemStack item) {
        if (p.getInventory().firstEmpty() == -1)
            p.getWorld().dropItemNaturally(p.getLocation(), item);
        else
            p.getInventory().addItem(new ItemStack[]{item});
    }

    //Etnfernt Items von der Hand
    public static void removeItemFromHand(Player p, int amount) {
        ItemStack hand = p.getItemInHand();

        if (amount <= 0)
            return;
        if ((hand == null) || (hand.getType() == Material.AIR))
            return;

        if (hand.getAmount() <= amount)
            p.setItemInHand(new ItemStack(Material.AIR));
        else
            hand.setAmount(hand.getAmount() - amount);
    }

    //Formartiert eine Zahl in 1.870.361
    public static String toConvert(double zahl) {

        DecimalFormat df = new DecimalFormat();
        String ausgabe = df.format(zahl);

        return ausgabe;
    }
}
