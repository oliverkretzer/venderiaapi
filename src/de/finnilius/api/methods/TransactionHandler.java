package de.finnilius.api.methods;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import de.finnilius.api.Main;
import de.finnilius.api.methods.API;
import de.finnilius.api.methods.ItemCreator;
import de.finnilius.api.mysql.MySQLMoney;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TransactionHandler implements Listener {
	private static TransactionHandler transactionHandler = new TransactionHandler();
	private Map<Player, Transaction> transactions;
	
	public static TransactionHandler get() {
		return transactionHandler;
		}
	
	public TransactionHandler() {
		transactions = new HashMap<Player, Transaction>();
		Main.getInstance().getServer().getPluginManager().registerEvents(this, Main.getInstance());
	}
	
	public void createTransaction(Player p, TransactionPerformer performer) {
		if (transactions.containsKey(p)) return;
		Transaction transaction = new Transaction(p, performer);
		transactions.put(p, transaction);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent clickEvent) {
	if (!(clickEvent.getWhoClicked() instanceof Player)) return;
	
	Player p = (Player)clickEvent.getWhoClicked();
	
	ItemStack item = clickEvent.getCurrentItem();
	if (clickEvent.getHotbarButton() != -1) {
		item = clickEvent.getView().getBottomInventory().getItem(clickEvent.getHotbarButton());
		if (item == null) {
			item = clickEvent.getCurrentItem();
			}
		}
	
	if ((item == null) || (item.getType() == Material.AIR)) return;
	
	if (clickEvent.getView().getTitle().equals("�8Transaktion")) {
		clickEvent.setCancelled(true);
		
		//Abfrage der Spieler eine Transaktion am laufen hat
		if (!transactions.containsKey(p)) {
			clickEvent.getView().close();
			p.sendMessage("�8(�6Venderia�8) �cEs l�uft gerade keine Transaktion.");
			return;
		}
		
		//Wenn der Spieler mit Bargeld zahlen will
		Transaction transaction = transactions.get(p);	
		
		if (clickEvent.getRawSlot() == 11) {
			double price = transaction.getPerformer().getPrice();
			
			if (MySQLMoney.getCash(p.getUniqueId().toString()) < price) {
				p.sendMessage("�8(�6Venderia�8) �cDu ben�tigst mindestens �7" + API.formatMoney(price) + " �cBargeld.");
				p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1.0F, 1.0F);
				clickEvent.getView().close();
				transactions.remove(p);
				return;
				}
			
			//Wenn er gen�gend Geld hat & die Transaktion erfolgreich ist
			p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2.0F, 2.0F);
			transactions.remove(p);
			transaction.getPerformer().paymentWithCash(transaction);
			MySQLMoney.removeCash(p.getUniqueId().toString(), price);
		}
		
		//Wenn er mit Karte zahlen will
		if (clickEvent.getRawSlot() == 15) {
			double money = transaction.getPerformer().getPrice();
			double tax = money * 0.05;
			double price = money + tax;
			
			if (MySQLMoney.getBank(p.getUniqueId().toString()) < price) {
				p.sendMessage("�8(�6Venderia�8) �cDu ben�tigst mindestens �7" + API.formatMoney(price) + " �cauf deinem Konto.");
				p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1.0F, 1.0F);
				transactions.remove(p);
				clickEvent.getView().close();
				return;
			}
			//Wenn er gen�gend Geld hat & die Transaktion erfolgreich ist
			p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2.0F, 2.0F);
			transactions.remove(p);
			transaction.getPerformer().paymentWithCard(transaction);
			MySQLMoney.removeBank(p.getUniqueId().toString(), price);
		}
	}
}
	
	@EventHandler
	public void onClose(InventoryCloseEvent closeEvent) {
		if (!(closeEvent.getPlayer() instanceof Player)) return;
		
		Player p = (Player)closeEvent.getPlayer();
		if (closeEvent.getView().getTitle().equals("�8Transaktion")) {
			if (!transactions.containsKey(p)) {
				return;
				}
			transactions.remove(p);
		}
	}
	
	public class Transaction {
		private Player player;
		private TransactionPerformer performer;
		
		public Transaction(Player player, TransactionPerformer performer) {
			this.player = player;
			this.performer = performer;
			openInventory(player, performer.getName(), performer.getLore(), performer.getPrice());
		}
		
		public Player getPlayer() {
			return player;
		}
		
		public TransactionPerformer getPerformer() {
			return performer;
		}
		
		public void openInventory(Player player, String name, String lore, double price) {	
			Inventory inventory = Bukkit.createInventory(null, 27, "�8Transaktion");
	
			ItemStack cash = API.getCustomSkull("http://textures.minecraft.net/texture/bf75d1b785d18d47b3ea8f0a7e0fd4a1fae9e7d323cf3b138c8c78cfe24ee59");
			SkullMeta cashMeta = (SkullMeta) cash.getItemMeta();
			cashMeta.setDisplayName("�6Bargeld");
			cashMeta.setLore(Arrays.asList(new String[] { "�7Preis: �f" + API.formatMoney(price),"", "�6Klicke, �7um mit Bargeld zu zahlen." }));
			cash.setItemMeta(cashMeta);
			double money = price;
			double newMoney = money * 0.05;
			ItemStack card = new ItemCreator().material(Material.PAPER).displayName("�6Karte").lore(Arrays.asList(new String[] { "�7Preis: �f" + API.formatMoney(money + newMoney) + " �f�o(5% Geb�hren)", "","�6Klicke, �7um mit Karte zu zahlen." })).build();		
			ItemStack item = new ItemCreator().material(Material.OAK_SIGN).displayName("�6" + name).lore(Arrays.asList(new String[] { "�7" + lore })).build();		
			
			inventory.setItem(11, cash);
			inventory.setItem(13, item);
			inventory.setItem(15, card);
		
			player.openInventory(inventory);
		}
	}
	
	public static abstract interface TransactionPerformer {
		public abstract double getPrice();
		public abstract String getName();
		public abstract String getLore();
		public abstract boolean paymentWithCard(Transaction paramTransaction);
		public abstract boolean paymentWithCash(Transaction paramTransaction);
	}
}