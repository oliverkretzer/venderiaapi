package de.finnilius.api.methods;

public class Message {

    private String receiver;
    private String message;
    private String date;

    public Message(String receiver, String message, String date) {
        this.receiver = receiver;
        this.message = message;
        this.date = date;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }
}