package de.finnilius.api.dbmodels;

public class BuyRegions {

    private String region;
    private double price;
    private String type;
    private String state;
    private String location;
    private String uuid;
    private double resellPrice;

    public BuyRegions(String region, double price, String type, String state, String location, String uuid, double resellPrice) {
        this.region = region;
        this.price = price;
        this.type = type;
        this.state = state;
        this.location = location;
        this.uuid = uuid;
        this.resellPrice = resellPrice;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getResellPrice() {
        return resellPrice;
    }

    public void setResellPrice(double resellPrice) {
        this.resellPrice = resellPrice;
    }
}
