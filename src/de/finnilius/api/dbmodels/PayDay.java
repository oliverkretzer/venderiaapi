package de.finnilius.api.dbmodels;

public class PayDay {

    private String uuid;
    private int time;
    private double trader;
    private double jobs;

    public PayDay(String uuid, int time, double trader, double jobs) {
        this.uuid = uuid;
        this.time = time;
        this.trader = trader;
        this.jobs = jobs;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getTrader() {
        return trader;
    }

    public void setTrader(double trader) {
        this.trader = trader;
    }

    public double getJobs() {
        return jobs;
    }

    public void setJobs(double jobs) {
        this.jobs = jobs;
    }
}
