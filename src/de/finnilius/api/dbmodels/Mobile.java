package de.finnilius.api.dbmodels;

public class Mobile {

    private String uuid;
    private String number;
    private String contacts;
    private String apps;

    public Mobile(String uuid, String number, String contacts, String apps) {
        this.uuid = uuid;
        this.number = number;
        this.contacts = contacts;
        this.apps = apps;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getApps() {
        return apps;
    }

    public void setApps(String apps) {
        this.apps = apps;
    }
}
