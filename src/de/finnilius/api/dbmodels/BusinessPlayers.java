package de.finnilius.api.dbmodels;

public class BusinessPlayers {

    private String uuid;
    private int id;
    private String name;
    private String rank;
    private double money;

    public BusinessPlayers(String uuid, int id, String name, String rank, double money) {
        this.uuid = uuid;
        this.id = id;
        this.name = name;
        this.rank = rank;
        this.money = money;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
