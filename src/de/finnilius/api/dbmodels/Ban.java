package de.finnilius.api.dbmodels;

public class Ban {

    private String uuid;
    private String time;
    private String punisher;
    private String reason;

    public Ban(String uuid, String time, String punisher, String reason) {
        this.uuid = uuid;
        this.time = time;
        this.punisher = punisher;
        this.reason = reason;
    }

    public String getUuid() {
        return uuid;
    }

    public String getTime() {
        return time;
    }

    public String getPunisher() {
        return punisher;
    }

    public String getReason() {
        return reason;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPunisher(String punisher) {
        this.punisher = punisher;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
