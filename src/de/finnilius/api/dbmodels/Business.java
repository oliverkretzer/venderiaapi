package de.finnilius.api.dbmodels;

public class Business {

    private int id;
    private String name;
    private String uuid;
    private String date;
    private String description;
    private String license;
    private int level;
    private double money;

    public Business(int id, String name, String uuid, String date, String description, String license, int level, double money) {
        this.id = id;
        this.name = name;
        this.uuid = uuid;
        this.date = date;
        this.description = description;
        this.license = license;
        this.level = level;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
