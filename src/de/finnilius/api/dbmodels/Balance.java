package de.finnilius.api.dbmodels;

public class Balance {

    private int id;
    private String uuid;
    private String receiver;
    private String date;
    private double money;
    private String type;
    private String reason;

    public Balance(int id, String uuid, String receiver, String date, double money, String type, String reason) {
        this.id = id;
        this.uuid = uuid;
        this.receiver = receiver;
        this.date = date;
        this.money = money;
        this.type = type;
        this.reason = reason;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
