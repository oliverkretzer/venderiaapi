package de.finnilius.api.dbmodels;

public class GroupPlayers {

    private String uuid;
    private String gruppe;

    public GroupPlayers(String uuid, String gruppe) {
        this.uuid = uuid;
        this.gruppe = gruppe;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getGruppe() {
        return gruppe;
    }

    public void setGruppe(String gruppe) {
        this.gruppe = gruppe;
    }
}
