package de.finnilius.api.dbmodels;

public class Groups {

    private String gruppe;
    private String uuid;
    private String date;

    public Groups(String gruppe, String uuid, String date) {
        this.gruppe = gruppe;
        this.uuid = uuid;
        this.date = date;
    }

    public String getGruppe() {
        return gruppe;
    }

    public void setGruppe(String gruppe) {
        this.gruppe = gruppe;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
