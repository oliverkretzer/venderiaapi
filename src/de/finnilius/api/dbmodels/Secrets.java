package de.finnilius.api.dbmodels;

public class Secrets {

    private int id;
    private String name;
    private String region;
    private String location;

    public Secrets(int id, String name, String region, String location) {
        this.id = id;
        this.name = name;
        this.region = region;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
