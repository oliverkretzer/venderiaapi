package de.finnilius.api.dbmodels;

public class BusinessShops {

    private int id;
    private int bid;
    private String name;
    private String location;
    private String date;
    private String item;
    private int slotID;
    private String available;

    public BusinessShops(int id, int bid, String name, String location, String date, String item, int slotID, String available) {
        this.id = id;
        this.bid = bid;
        this.name = name;
        this.location = location;
        this.date = date;
        this.item = item;
        this.slotID = slotID;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }
}
