package de.finnilius.api.dbmodels;

public class Packages {

    private int id;
    private String toUUID;
    private String fromUUID;
    private String size;
    private String date;
    private String inventory;

    public Packages(int id, String toUUID, String fromUUID, String size, String date, String inventory) {
        this.id = id;
        this.toUUID = toUUID;
        this.fromUUID = fromUUID;
        this.size = size;
        this.date = date;
        this.inventory = inventory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToUUID() {
        return toUUID;
    }

    public void setToUUID(String toUUID) {
        this.toUUID = toUUID;
    }

    public String getFromUUID() {
        return fromUUID;
    }

    public void setFromUUID(String fromUUID) {
        this.fromUUID = fromUUID;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }
}
