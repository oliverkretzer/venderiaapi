package de.finnilius.api.dbmodels;

public class Punishments {

    private int id;
    private String uuid;
    private String reason;
    private String punisher;
    private String date;

    public Punishments(int id, String uuid, String reason, String punisher, String date) {
        this.id = id;
        this.uuid = uuid;
        this.reason = reason;
        this.punisher = punisher;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPunisher() {
        return punisher;
    }

    public void setPunisher(String punisher) {
        this.punisher = punisher;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
