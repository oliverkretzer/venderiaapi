package de.finnilius.api.dbmodels;

public class Stats {

    private String uuid;
    private int level;
    private int exp;
    private int playtime;
    private String npcs;

    public Stats(String uuid, int level, int exp, int playtime, String npcs) {
        this.uuid = uuid;
        this.level = level;
        this.exp = exp;
        this.playtime = playtime;
        this.npcs = npcs;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getPlaytime() {
        return playtime;
    }

    public void setPlaytime(int playtime) {
        this.playtime = playtime;
    }

    public String getNpcs() {
        return npcs;
    }

    public void setNpcs(String npcs) {
        this.npcs = npcs;
    }
}
