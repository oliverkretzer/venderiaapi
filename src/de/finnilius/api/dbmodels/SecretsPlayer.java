package de.finnilius.api.dbmodels;

public class SecretsPlayer {

    private String uuid;
    private String ids;
    private int amount;

    public SecretsPlayer(String uuid, String ids, int amount) {
        this.uuid = uuid;
        this.ids = ids;
        this.amount = amount;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
