package de.finnilius.api.dbmodels;

public class BusinessBuild {

    private int id;
    private String name;
    private String fromUUID;
    private String toUUID;
    private String status;
    private String description;
    private String license;
    private int level;
    private double money;
    private double ceo;
    private double employee;

    public BusinessBuild(int id, String name, String fromUUID, String toUUID, String status, String description, String license, int level, double money, double ceo, double employee) {
        this.id = id;
        this.name = name;
        this.fromUUID = fromUUID;
        this.toUUID = toUUID;
        this.status = status;
        this.description = description;
        this.license = license;
        this.level = level;
        this.money = money;
        this.ceo = ceo;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFromUUID() {
        return fromUUID;
    }

    public void setFromUUID(String fromUUID) {
        this.fromUUID = fromUUID;
    }

    public String getToUUID() {
        return toUUID;
    }

    public void setToUUID(String toUUID) {
        this.toUUID = toUUID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getCeo() {
        return ceo;
    }

    public void setCeo(double ceo) {
        this.ceo = ceo;
    }

    public double getEmployee() {
        return employee;
    }

    public void setEmployee(double employee) {
        this.employee = employee;
    }
}
