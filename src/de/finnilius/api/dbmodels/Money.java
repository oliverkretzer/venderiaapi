package de.finnilius.api.dbmodels;

public class Money {

    private String uuid;
    private double cash;
    private double bank;
    private String number;

    public Money(String uuid, double cash, double bank, String number) {
        this.uuid = uuid;
        this.cash = cash;
        this.bank = bank;
        this.number = number;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getBank() {
        return bank;
    }

    public void setBank(double bank) {
        this.bank = bank;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
