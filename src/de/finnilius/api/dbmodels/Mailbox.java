package de.finnilius.api.dbmodels;

public class Mailbox {

    private String uuid;
    private String region;
    private int x;
    private int y;
    private int z;

    public Mailbox(String uuid, String region, int x, int y, int z) {
        this.uuid = uuid;
        this.region = region;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
