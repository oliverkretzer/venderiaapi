package de.finnilius.api.dbmodels;

public class Tickets {

    private String uuid;
    private String type;
    private String time;

    public Tickets(String uuid, String type, String time) {
        this.uuid = uuid;
        this.type = type;
        this.time = time;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
