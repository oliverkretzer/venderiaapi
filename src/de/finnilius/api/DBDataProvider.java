package de.finnilius.api;

import de.finnilius.api.dbmodels.*;

import java.util.ArrayList;
import java.util.List;

public class DBDataProvider {

    private List<Ban> ban;
    private List<Punishments> punishments;
    private List<Business> businesses;
    private List<BusinessBank> businessBanks;
    private List<BusinessBuild> businessBuilds;
    private List<BusinessPlayers> businessPlayers;
    private List<BusinessShops> businessShops;
    private List<Groups> groups;
    private List<GroupPlayers> groupPlayers;
    private List<Mailbox> mailboxes;
    private List<Packages> packages;
    private List<Messages> messages;
    private List<Mobile> mobiles;
    private List<Money> money;
    private List<Balance> balances;
    private List<PayDay> payDays;
    private List<Secrets> secrets;
    private List<SecretsPlayer> secretsPlayers;
    private List<Stats> stats;
    private List<Tickets> tickets;
    private List<RentRegions> rentRegions;
    private List<BuyRegions> buyRegions;

    public DBDataProvider() {
        this.ban = new ArrayList<>();
        this.punishments = new ArrayList<>();
        this.businesses = new ArrayList<>();
        this.businessBanks = new ArrayList<>();
        this.businessBuilds = new ArrayList<>();
        this.businessPlayers = new ArrayList<>();
        this.businessShops = new ArrayList<>();
        this.groups = new ArrayList<>();
        this.groupPlayers = new ArrayList<>();
        this.mailboxes = new ArrayList<>();
        this.packages = new ArrayList<>();
        this.messages = new ArrayList<>();
        this.mobiles = new ArrayList<>();
        this.money = new ArrayList<>();
        this.balances = new ArrayList<>();
        this.payDays = new ArrayList<>();
        this.secrets = new ArrayList<>();
        this.secretsPlayers = new ArrayList<>();
        this.stats = new ArrayList<>();
        this.tickets = new ArrayList<>();
        this.rentRegions = new ArrayList<>();
        this.buyRegions = new ArrayList<>();
    }

    public List<Ban> getBan() {
        return ban;
    }

    public List<Punishments> getPunishments() {
        return punishments;
    }

    public List<Business> getBusinesses() {
        return businesses;
    }

    public List<BusinessBank> getBusinessBanks() {
        return businessBanks;
    }

    public List<BusinessBuild> getBusinessBuilds() {
        return businessBuilds;
    }

    public List<BusinessPlayers> getBusinessPlayers() {
        return businessPlayers;
    }

    public List<BusinessShops> getBusinessShops() {
        return businessShops;
    }

    public List<Groups> getGroups() {
        return groups;
    }

    public List<GroupPlayers> getGroupPlayers() {
        return groupPlayers;
    }

    public List<Mailbox> getMailboxes() {
        return mailboxes;
    }

    public List<Packages> getPackages() {
        return packages;
    }

    public List<Messages> getMessages() {
        return messages;
    }

    public List<Mobile> getMobiles() {
        return mobiles;
    }

    public List<Money> getMoney() {
        return money;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    public List<PayDay> getPayDays() {
        return payDays;
    }

    public List<Secrets> getSecrets() {
        return secrets;
    }

    public List<SecretsPlayer> getSecretsPlayers() {
        return secretsPlayers;
    }

    public List<Stats> getStats() {
        return stats;
    }

    public List<Tickets> getTickets() {
        return tickets;
    }

    public List<RentRegions> getRentRegions() {
        return rentRegions;
    }

    public List<BuyRegions> getBuyRegions() {
        return buyRegions;
    }
}
