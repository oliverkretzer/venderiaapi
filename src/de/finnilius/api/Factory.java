package de.finnilius.api;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Factory {

    private File file;
    private FileConfiguration config;
    private Connection connection;
    private String HOST, DATABASE, USER, PASSWORD;
    private ExecutorService executor;

    public Factory() {
        this.file = createConfig();
        this.config = createFileConfiguration(this.file);
        this.executor = Executors.newCachedThreadPool();
        createStandardConfig();
        readData();
    }

    private File createConfig() {
        return new File("plugins/venderiaAPI", "mysql.yml");
    }

    private FileConfiguration createFileConfiguration(File file) {
        return YamlConfiguration.loadConfiguration(file);
    }

    public void createStandardConfig() {
        this.config.options().copyDefaults(true);

        this.config.addDefault("Host", "Host");
        this.config.addDefault("Database", "Datenbank");
        this.config.addDefault("User", "Username");
        this.config.addDefault("Passwort", "Passwort");

        try {
            this.config.save(this.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readData() {
        this.HOST = this.config.getString("Host");
        this.DATABASE = this.config.getString("Database");
        this.USER = this.config.getString("User");
        this.PASSWORD = this.config.getString("Passwort");
    }

    public void connect() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://" + HOST + ":3306/" + DATABASE + "?autoReconnect=true", USER, PASSWORD);
            System.out.println("§aMYSQL> Die Verbindung zur Datenbank wurde hergestellt!");
        } catch (SQLException e) {
            System.out.println("§cMYSQL> Die Verbindung zur Datenbank ist fehlgeschlagen! Fehler: " + e.getMessage());
        }
    }

    public void disconnect() {
        try {
            if (connection!=null && !connection.isClosed()){
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void createTables() {
        try {
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Secrets (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, Name VARCHAR(1000), Region VARCHAR(100), Location VARCHAR(100), PRIMARY KEY(`ID`))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS SecretsPlayer (UUID VARCHAR(100),  IDs VARCHAR(100), Amount INT)");

            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS RentRegions(Region VARCHAR(100), Price DOUBLE, Type VARCHAR(100), State VARCHAR(100), Location VARCHAR(100), UUID VARCHAR(64), Duration Long)");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS BuyRegions(Region VARCHAR(100), Price DOUBLE, Type VARCHAR(100), State VARCHAR(100), Location VARCHAR(100), UUID VARCHAR(64), Resellprice DOUBLE)");

            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Business (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, Name VARCHAR(100), UUID VARCHAR(36), Date VARCHAR(100), Description VARCHAR(1000), License VARCHAR(100), Level INT, Money DOUBLE, PRIMARY KEY(`ID`))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS BusinessPlayers (UUID VARCHAR(36), ID INT(100), Name VARCHAR(100), Rank VARCHAR(100), Money DOUBLE)");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS BusinessBank (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, Name VARCHAR(100), FromUUID VARCHAR(36), ToUUID VARCHAR(36), Status VARCHAR(100), Description VARCHAR(1000), License VARCHAR(100), Level INT, Money DOUBLE, CEO DOUBLE, Employee DOUBLE, PRIMARY KEY(`ID`))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS BusinessBuild (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, Name VARCHAR(100), FromUUID VARCHAR(36), ToUUID VARCHAR(36), Status VARCHAR(100), Description VARCHAR(1000), License VARCHAR(100), Level INT, Money DOUBLE, CEO DOUBLE, Employee DOUBLE, PRIMARY KEY(`ID`))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS BusinessShops (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, BID INT, Name VARCHAR(100), Location VARCHAR(100), Date VARCHAR(100), Item VARCHAR(100), Sold INT, Available VARCHAR(1000), PRIMARY KEY(`ID`))");

            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Mailbox (UUID VARCHAR(100), Region VARCHAR(100), X INT(100), Y INT(100), Z INT(100))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Packages (ID VARCHAR(100), ToUUID VARCHAR(100), FromUUID VARCHAR(100), Size VARCHAR(100), Date VARCHAR(100), Inventory TEXT(50000))");

            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Groups (Gruppe VARCHAR(100), UUID VARCHAR(100), Date VARCHAR(100))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS GroupPlayers (UUID VARCHAR(100), Gruppe VARCHAR(100))");

            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Money (UUID VARCHAR(100), Cash DOUBLE, Bank DOUBLE, Number VARCHAR(100))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Balance (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, UUID VARCHAR(100), Receiver VARCHAR(100), Date VARCHAR(100), Money DOUBLE, Type VARCHAR(100), Reason VARCHAR(100), PRIMARY KEY(`ID`))");

            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Tickets (UUID VARCHAR(100), Type VARCHAR(100), Time VARCHAR(10000))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS PayDay (UUID VARCHAR(100), Time INT, Trader DOUBLE, Jobs DOUBLE)");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Stats (UUID VARCHAR(100), Level INT, EXP INT, Playtime INT, NPCs VARCHAR(1000))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Mobile (UUID VARCHAR(100), Number VARCHAR(100), Contacts VARCHAR(5000), Apps VARCHAR(5000))");

            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Ban (UUID VARCHAR(100), Time VARCHAR(100), Punisher VARCHAR(100), Reason VARCHAR(100))");
            this.connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Punishments (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, UUID VARCHAR(100), Reason VARCHAR(100), Punisher VARCHAR(100), Date VARCHAR(100), PRIMARY KEY(`ID`))");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void update(PreparedStatement stat) {
        if (isConnected()) {
            executor.execute(() -> queryUpdate(stat));
        }
    }

    public void update(String stat) {
        if (isConnected()) {
            executor.execute(() -> queryUpdate(stat));
        }
    }

    public void queryUpdate(String query) {
        if (isConnected()) {
            try (PreparedStatement statment = this.connection.prepareStatement(query)) {
                queryUpdate(statment);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void queryUpdate(PreparedStatement stmt) {
        if (isConnected()) {
            try {
                stmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ResultSet query(String query) {
        if (isConnected()) {
            try {
                return query(this.connection.prepareStatement(query));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private ResultSet query(PreparedStatement stat) {
        if (isConnected()) {
            try {
                return stat.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private ResultSet asyncQuery(PreparedStatement stmt) {
        if (isConnected()) {
            Future<ResultSet> future = executor.submit(() -> query(stmt));
            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public ResultSet asyncQuery(String statement) {
        if (isConnected()) {
            Future<ResultSet> future = executor.submit(() -> query(statement));
            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    private boolean isConnected() {
        try {
            if (this.connection == null || !this.connection.isValid(10) || this.connection.isClosed()) {
                return false;
            }
        } catch (SQLException e) {

            return false;
        }
        return true;
    }
}
