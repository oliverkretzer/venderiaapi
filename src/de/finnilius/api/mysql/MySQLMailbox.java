package de.finnilius.api.mysql;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.finnilius.api.dbmodels.GroupPlayers;
import de.finnilius.api.dbmodels.Groups;
import de.finnilius.api.dbmodels.Mailbox;
import de.finnilius.api.dbmodels.Packages;
import org.bukkit.Location;
import org.bukkit.inventory.Inventory;
import de.finnilius.api.Main;

public class MySQLMailbox {

    public static void loadData() {
        ResultSet mailboxData = Main.getFactory().query("SELECT * FROM Mailbox");
        ResultSet packagesData = Main.getFactory().query("SELECT * FROM Packages");
        try {
            while (mailboxData.next()) {
                Main.getDbDataProvider().getMailboxes().add(new Mailbox(
                        mailboxData.getString("UUID"),
                        mailboxData.getString("Region"),
                        mailboxData.getInt("X"),
                        mailboxData.getInt("Y"),
                        mailboxData.getInt("Z")
                ));
            }
            while (packagesData.next()) {
                Main.getDbDataProvider().getPackages().add(new Packages(
                        packagesData.getInt("ID"),
                        packagesData.getString("ToUUID"),
                        packagesData.getString("FromUUID"),
                        packagesData.getString("Size"),
                        packagesData.getString("Date"),
                        packagesData.getString("Inventory")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getMailboxes().size() + " Mailboxes geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getPackages().size() + " Packages geladen.");
    }

    private static Mailbox getMailboxByUUID(String uuid) {
        for (Mailbox mailbox : Main.getDbDataProvider().getMailboxes()) {
            if (mailbox.getUuid().equals(uuid)) return mailbox;
        }
        return null;
    }

    private static Mailbox getMailboxByLocation(Location location) {
        for (Mailbox mailbox : Main.getDbDataProvider().getMailboxes()) {
            if (location.getX() == mailbox.getX() &&
                    location.getY() == mailbox.getY() &&
                    location.getZ() == mailbox.getZ())
                return mailbox;
        }
        return null;
    }

    private static boolean isValidMailbox(Location location) {
        return getMailboxByLocation(location) != null;
    }

    //Checkt ob der Spieler bereits einen Briefkasten hat
    public static boolean hasMailbox(String uuid) {
        return getMailboxByUUID(uuid) != null;
    }

    //Der Owner des Briefkastens
    public static String getOwner(Location location) {
        if (!isValidMailbox(location)) return null;
        return getMailboxByLocation(location).getUuid();
    }

    //F�gt einen Briefkasten zur Datenbank hinzu
    public static void addMailbox(String playerUUID, String region, Location location) {
        Main.getDbDataProvider().getMailboxes().add(new Mailbox(playerUUID, region, (int) location.getX(), (int) location.getY(), (int) location.getZ()));
        Main.getFactory().update("INSERT INTO Mailbox(UUID, Region, X, Y, Z) VALUES ('" + playerUUID + "', '" + region + "', '" + location.getX() + "', '" + location.getY() + "', '" + location.getZ() + "');");
    }

    //Entfernt einen Briefkasten aus der Datenbank
    public static void removeMailbox(String playerUUID, Location location) {
        Main.getDbDataProvider().getMailboxes().remove(getMailboxByLocation(location));
        Main.getFactory().update("DELETE FROM Mailbox WHERE UUID = '" + playerUUID + "' AND X = '" + location.getX() + "' AND Y = '" + location.getY() + "' AND Z = '" + location.getZ() + "'");
    }

    //Alle Briefk�sten die in der Datenbank sind
    public static boolean isMailbox(Location location) {
        return isValidMailbox(location);
    }

    //Alle Pakete von einem Spieler
    public static ArrayList<Integer> getPackages(String playerUUID) {
        ArrayList<Integer> packages = new ArrayList<>();
        for (Packages pack : Main.getDbDataProvider().getPackages()) {
            if (pack.getToUUID().equals(playerUUID)) {
                packages.add(pack.getId());
            }
        }
        return packages;
    }

    private static Packages getPackageByID(int id) {
        for (Packages packages : Main.getDbDataProvider().getPackages()) {
            if (packages.getId() == id) {
                return packages;
            }
        }
        return null;
    }

    private static boolean isValidPackage(int id) {
        return getPackageByID(id) != null;
    }

    //Inhalt eines Paketes
    public static Inventory getPackage(Integer packageID) {
        if (!isValidPackage(packageID)) return null;
        try {
            return Serialization.fromBase64(getPackageByID(packageID).getInventory());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Spieler der das Paket gesendet hat
    public static String getFromUUID(Integer packageID) {
        if (!isValidPackage(packageID)) return null;
        return getPackageByID(packageID).getFromUUID();
    }

    //Spieler der das Paket erhalten hat
    public static String getToUUID(Integer packageID) {
        if (!isValidPackage(packageID)) return null;
        return getPackageByID(packageID).getToUUID();
    }

    //Spieler der das Paket erhalten hat
    public static String getSize(Integer packageID) {
        if (!isValidPackage(packageID)) return null;
        return getPackageByID(packageID).getSize();
    }

    //Datum wann das Paket gesendet wurde
    public static String getDate(Integer packageID) {
        if (!isValidPackage(packageID)) return null;
        return getPackageByID(packageID).getDate();
    }

    //Sendet ein Paket ab
    public static void sendPackage(String fromUUID, String toUUID, Integer packageID, String date) {
        if (isValidPackage(packageID)) return;
        Packages packages = getPackageByID(packageID);
        packages.setFromUUID(fromUUID);
        packages.setToUUID(toUUID);
        packages.setDate(date);
        Main.getFactory().update("UPDATE Packages SET FromUUID= '" + fromUUID + "' WHERE ID= '" + packageID + "'");
        Main.getFactory().update("UPDATE Packages SET ToUUID= '" + toUUID + "' WHERE ID= '" + packageID + "'");
        Main.getFactory().update("UPDATE Packages SET Date= '" + date + "' WHERE ID= '" + packageID + "'");
    }

    //Erstellt ein Paket in der Datenbank
    public static void createPackage(Integer packageID, String size, Inventory inventory) {
        Main.getDbDataProvider().getPackages().add(new Packages(packageID, "", "", size, "", Serialization.toBase64(inventory)));
        Main.getFactory().update("INSERT INTO Packages(ID, ToUUID, FromUUID, Size, Date, Inventory) VALUES ('" + packageID + "', '" + "" + "', '" + "" + "', '" + size + "', '" + "" + "', '" + Serialization.toBase64(inventory) + "');");
    }

    //Sichert den Inhalt eines Pakets
    public static void savePackage(Integer packageID, Inventory inventory) {
        if(!isValidPackage(packageID)) return;
        getPackageByID(packageID).setInventory(Serialization.toBase64(inventory));
        Main.getFactory().update("UPDATE Packages SET Inventory = '" + Serialization.toBase64(inventory) + "' WHERE ID= '" + packageID + "'");
    }

    //Entfernt ein Paket aus der Datenbank
    public static void removePackage(Integer packageID) {
        if(!isValidPackage(packageID)) return;
        Main.getDbDataProvider().getPackages().remove(getPackageByID(packageID));
        Main.getFactory().update("DELETE FROM Packages WHERE ID = '" + packageID + "'");
    }
}
