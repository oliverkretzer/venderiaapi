package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import de.finnilius.api.dbmodels.Balance;
import de.finnilius.api.dbmodels.GroupPlayers;
import de.finnilius.api.dbmodels.Groups;
import de.finnilius.api.dbmodels.Money;
import org.bukkit.Bukkit;

import de.finnilius.api.Main;

public class MySQLMoney {

    private static int lastBalanceID = 0;

    public static void loadData() {
        ResultSet moneyData = Main.getFactory().query("SELECT * FROM Money");
        ResultSet balanceData = Main.getFactory().query("SELECT * FROM Balance");
        try {
            while (moneyData.next()) {
                Main.getDbDataProvider().getMoney().add(new Money(
                        moneyData.getString("UUID"),
                        moneyData.getDouble("Cash"),
                        moneyData.getDouble("Bank"),
                        moneyData.getString("Number")
                ));
            }
            while (balanceData.next()) {
                Main.getDbDataProvider().getBalances().add(new Balance(
                        balanceData.getInt("ID"),
                        balanceData.getString("UUID"),
                        balanceData.getString("Receiver"),
                        balanceData.getString("Date"),
                        balanceData.getDouble("Money"),
                        balanceData.getString("Type"),
                        balanceData.getString("Reason")
                ));
                lastBalanceID = balanceData.getInt("ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getMoney().size() + " Money geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBalances().size() + " Balances geladen.");
    }

    private static Money getMoneyByUUID(String uuid) {
        for (Money money : Main.getDbDataProvider().getMoney()) {
            if (money.getUuid().equals(uuid)) return money;
        }
        return null;
    }

    public static boolean playerExists(String playerUUID) {
        return getMoneyByUUID(playerUUID) != null;
    }

    public static String createBankNumber() {
        String number = "DE";
        Random value = new Random();
        int r1 = value.nextInt(10);
        int r2 = value.nextInt(10);
        number += Integer.toString(r1) + Integer.toString(r2) + " ";

        int count = 0;
        int n = 0;
        for (int i = 0; i < 12; i++) {
            if (count == 4) {
                number += " ";
                count = 0;
            } else
                n = value.nextInt(10);
            number += Integer.toString(n);
            count++;
        }
        return number;
    }

    public static void createPlayer(String playerUUID) {
        if (!playerExists(playerUUID)) {
            double konto = 0.00;
            double bar = 0.00;
            String kontonummer = createBankNumber();
            Main.getDbDataProvider().getMoney().add(new Money(playerUUID, bar, konto, kontonummer));
            Main.getFactory().update("INSERT INTO Money (UUID, Cash, Bank, Number) VALUES ('" + playerUUID + "', '" + bar + "', '" + konto + "', '" + kontonummer + "')");
        }
    }

    public static String getNumber(String playerUUID) {
        if (!playerExists(playerUUID)) return null;
        return getMoneyByUUID(playerUUID).getNumber();
    }

    public static void setCash(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            getMoneyByUUID(playerUUID).setCash(amount);
            Main.getFactory().update("UPDATE Money SET Cash='" + amount + "' WHERE UUID='" + playerUUID + "'");
        }
    }

    public static void setBank(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            getMoneyByUUID(playerUUID).setBank(amount);
            Main.getFactory().update("UPDATE Money SET Bank='" + amount + "' WHERE UUID='" + playerUUID + "'");
        }
    }

    public static Double getCash(String playerUUID) {
        if (!playerExists(playerUUID)) return 0.00;
        return getMoneyByUUID(playerUUID).getCash();
    }

    public static Double getBank(String playerUUID) {
        if (!playerExists(playerUUID)) return 0.00;
        return getMoneyByUUID(playerUUID).getBank();
    }

    public static void addCash(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            setCash(playerUUID, getCash(playerUUID) + amount);
        }
    }

    public static void addBank(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            setBank(playerUUID, getBank(playerUUID) + amount);
        }
    }

    public static void removeCash(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            setCash(playerUUID, getCash(playerUUID) - amount);
        }
    }

    public static void removeBank(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            setBank(playerUUID, getBank(playerUUID) - amount);
        }
    }

    private static Balance getBalanceByID(int id) {
        for (Balance balance : Main.getDbDataProvider().getBalances()) {
            if (balance.getId() == id) return balance;
        }
        return null;
    }

    private static boolean isValidID(int id){
        return getBalanceByID(id) != null;
    }

    public static void createBalance(String playerUUID, String receiverUUID, String date, double money, String type, String reason) {
        //("CREATE TABLE IF NOT EXISTS Balance (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, UUID VARCHAR(100), Receiver VARCHAR(100), Date VARCHAR(100), Money DOUBLE, Type VARCHAR(100), Reason VARCHAR(100), PRIMARY KEY(`ID`))");
        lastBalanceID++;
        Main.getDbDataProvider().getBalances().add(new Balance(lastBalanceID, playerUUID, receiverUUID, date, money, type, reason));
        Main.getFactory().update("INSERT INTO Balance (UUID, Receiver, Date, Money, Type, Reason) VALUES ('" + playerUUID + "', '" + receiverUUID + "', '" + date + "', '" + money + "', '" + type + "', '" + reason + "')");
    }

    public static ArrayList<String> getBalance(String playerUUID) {
        ArrayList<String> balance = new ArrayList<>();
        for (Balance b : Main.getDbDataProvider().getBalances()) {
            if(b.getUuid().equals(playerUUID)){
                balance.add("" + b.getId());
            }
        }
        return balance;
    }

    public static String getDate(Integer balanceID) {
        if(!isValidID(balanceID)) return null;
        return getBalanceByID(balanceID).getDate();
    }

    public static String getReceiver(Integer balanceID) {
        if(!isValidID(balanceID)) return null;
        return getBalanceByID(balanceID).getReceiver();
    }

    public static String getType(Integer balanceID) {
        if(!isValidID(balanceID)) return null;
        return getBalanceByID(balanceID).getType();
    }

    public static String getReason(Integer balanceID) {
        if(!isValidID(balanceID)) return null;
        return getBalanceByID(balanceID).getReason();
    }

    public static String getAmount(Integer balanceID) {
        if(!isValidID(balanceID)) return null;
        return "" + getBalanceByID(balanceID).getMoney();
    }
}
