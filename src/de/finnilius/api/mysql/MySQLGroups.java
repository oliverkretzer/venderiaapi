package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import de.finnilius.api.dbmodels.Ban;
import de.finnilius.api.dbmodels.GroupPlayers;
import de.finnilius.api.dbmodels.Groups;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import de.finnilius.api.Main;


public class MySQLGroups {

    public static void loadData() {
        ResultSet groupData = Main.getFactory().query("SELECT * FROM Groups");
        ResultSet groupPlayerData = Main.getFactory().query("SELECT * FROM GroupPlayers");
        try {
            while (groupData.next()) {
                Main.getDbDataProvider().getGroups().add(new Groups(
                        groupData.getString("Gruppe"),
                        groupData.getString("UUID"),
                        groupData.getString("Date")
                ));
            }
            while (groupPlayerData.next()) {
                Main.getDbDataProvider().getGroupPlayers().add(new GroupPlayers(
                        groupPlayerData.getString("UUID"),
                        groupPlayerData.getString("Gruppe")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getGroups().size() + " Gruppen geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBusinessPlayers().size() + " Gruppen-Spieler geladen.");
    }

    private static Groups getGroupByName(String groupName) {
        for (Groups groups : Main.getDbDataProvider().getGroups()) {
            if (groups.getGruppe().equals(groupName)) return groups;
        }
        return null;
    }

    private static boolean isValidGroupName(String groupName) {
        return getGroupByName(groupName) != null;
    }

    //Gruppe existiert
    public static boolean groupExists(String groupName) {
        return isValidGroupName(groupName);
    }

    //Gruppe erstellen
    public static void createGroup(String playerUUID, String date, String groupName) {
        Main.getDbDataProvider().getGroups().add(new Groups(groupName, playerUUID, date));
        Main.getFactory().update("INSERT INTO Groups(Gruppe, UUID, Date) VALUES ('" + groupName + "', '" + playerUUID + "', '" + date + "');");
    }

    //Gruppe l�schen
    public static void deleteGroup(String groupName) {
        Main.getDbDataProvider().getGroups().remove(getGroupByName(groupName));
        Main.getFactory().update("DELETE FROM Groups WHERE Gruppe='" + groupName + "'");
    }

    //Ist der Spieler in eine Gruppe
    public static boolean hasGroup(String playerUUID) {
        return getGroupWithPlayer(playerUUID) != null;
    }

    //Gruppen Name mit Spieler
    public static String getGroupWithPlayer(String playerUUID) {
        for (GroupPlayers groupPlayers : Main.getDbDataProvider().getGroupPlayers()) {
            if (groupPlayers.getUuid().equals(playerUUID)) return groupPlayers.getGruppe();
        }
        return null;
    }

    //Gruppen Name mit Spieler
    public static GroupPlayers getGroupWithUUID(String playerUUID) {
        for (GroupPlayers groupPlayers : Main.getDbDataProvider().getGroupPlayers()) {
            if (groupPlayers.getUuid().equals(playerUUID)) return groupPlayers;
        }
        return null;
    }

    //Alle Mitglieder
    public static ArrayList<String> getMember(String groupName) {
        ArrayList<String> member = new ArrayList<>();
        for (GroupPlayers groupPlayers : Main.getDbDataProvider().getGroupPlayers()) {
            if(groupPlayers.getGruppe().equals(groupName)){
                member.add(groupPlayers.getUuid());
            }
        }
        return member;
    }

    //Mitglied hinzuf�gen
    public static void addMember(String playerUUID, String groupName) {
        Main.getDbDataProvider().getGroupPlayers().add(new GroupPlayers(playerUUID, groupName));
        Main.getFactory().update("INSERT INTO GroupPlayers(UUID, Gruppe) VALUES ('" + playerUUID + "', '" + groupName + "');");
    }

    //Mitglied entfernen
    public static void removeMember(String playerUUID) {
        Main.getDbDataProvider().getGroupPlayers().remove(getGroupWithUUID(playerUUID));
        Main.getFactory().update("DELETE FROM GroupPlayers WHERE UUID = '" + playerUUID + "'");
    }

    //Besitzer der Gruppe
    public static String getOwner(String groupName) {
        if (!isValidGroupName(groupName)) return null;
        return getGroupByName(groupName).getUuid();
    }

    //Erstelldatum der Gruppe
    public static String getDate(String groupName) {
        if (!isValidGroupName(groupName)) return null;
        return getGroupByName(groupName).getDate();
    }

    //Name der Gruppe
    public static String getName(String groupName) {
        if (!isValidGroupName(groupName)) return null;
        return getGroupByName(groupName).getGruppe();
    }

    //Namen setzten
    public static void setName(String groupName, String newGroupName) {
        getGroupByName(groupName).setGruppe(newGroupName);
        for (GroupPlayers players : Main.getDbDataProvider().getGroupPlayers()){
            if(players.getGruppe().equals(groupName)){
               players.setGruppe(newGroupName);
            }
        }
        Main.getFactory().update("UPDATE Groups SET Gruppe= '" + newGroupName + "' WHERE Gruppe= '" + groupName + "'");
        Main.getFactory().update("UPDATE GroupPlayers SET Gruppe = '" + newGroupName + "' WHERE Gruppe = '" + groupName + "'");
    }
}
