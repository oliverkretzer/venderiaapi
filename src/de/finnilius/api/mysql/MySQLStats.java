package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import de.finnilius.api.dbmodels.Ban;
import de.finnilius.api.dbmodels.Stats;
import org.bukkit.Bukkit;

import de.finnilius.api.Main;

public class MySQLStats {

    public static void loadData() {
        ResultSet statsData = Main.getFactory().query("SELECT * FROM Stats");
        try {
            while (statsData.next()) {
                Main.getDbDataProvider().getStats().add(new Stats(
                        statsData.getString("UUID"),
                        statsData.getInt("Level"),
                        statsData.getInt("EXP"),
                        statsData.getInt("Playtime"),
                        statsData.getString("NPCs")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getStats().size() + " Stats geladen.");
    }

    private static Stats getStatsByUUID(String uuid) {
        for (Stats stats : Main.getDbDataProvider().getStats()) {
            if (stats.getUuid().equals(uuid)) {
                return stats;
            }
        }
        return null;
    }

    public static boolean playerExists(String playerUUID) {
        return getStatsByUUID(playerUUID) != null;
    }

    public static void createPlayer(String playerUUID) {
        if (!playerExists(playerUUID)) {
            Main.getDbDataProvider().getStats().add(new Stats(playerUUID, 1, 0, 0, ""));
            Main.getFactory().update("INSERT INTO Stats (UUID, Level, EXP, Playtime, NPCs) VALUES ('" + playerUUID + "', '" + 1 + "', '" + 0 + "', '" + 0 + "','')");
        }

    }

    public static void addTime(String playerUUID) {
        if (playerExists(playerUUID)) {
            setTime(playerUUID, getTime(playerUUID) + 1);
        }
    }

    public static void setTime(String playerUUID, int playtime) {
        if (playerExists(playerUUID)) {
            getStatsByUUID(playerUUID).setPlaytime(playtime);
            Main.getFactory().update("UPDATE Stats SET Playtime = '" + playtime + "' WHERE UUID = '" + playerUUID + "'");
        }
    }

    public static int getTime(String playerUUID) {
        if(!playerExists(playerUUID)) return -1;
        return getStatsByUUID(playerUUID).getPlaytime();
    }

    public static String getNPCs(String playerUUID) {
        if(!playerExists(playerUUID)) return null;
        return getStatsByUUID(playerUUID).getNpcs();
    }

    public static void addNPC(String playerUUID, String newNPC) {
        if(!playerExists(playerUUID)) return;

        String npcsAsList = getNPCs(playerUUID);
        npcsAsList = npcsAsList + newNPC + ";";

        getStatsByUUID(playerUUID).setNpcs(npcsAsList);
        Main.getFactory().update("UPDATE Stats SET NPCs= '" + npcsAsList + "' WHERE UUID= '" + playerUUID + "'");
    }

    public static void setLevel(String playerUUID, int amount) {
        if (playerExists(playerUUID)) {
            getStatsByUUID(playerUUID).setLevel(amount);
            Main.getFactory().update("UPDATE Stats SET Level='" + amount + "' WHERE UUID='" + playerUUID + "'");
        }
    }

    public static Integer getLevel(String playerUUID) {
        if(!playerExists(playerUUID)) return -1;
        return getStatsByUUID(playerUUID).getLevel();
    }

    public static void setEXP(String playerUUID, int amount) {
        if (playerExists(playerUUID)) {
            getStatsByUUID(playerUUID).setExp(amount);
            Main.getFactory().update("UPDATE Stats SET EXP='" + amount + "' WHERE UUID='" + playerUUID + "'");
        }
    }

    public static Integer getEXP(String playerUUID) {
        if(!playerExists(playerUUID)) return -1;
        return getStatsByUUID(playerUUID).getExp();
    }
}
