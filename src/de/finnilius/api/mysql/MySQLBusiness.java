package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.finnilius.api.dbmodels.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import de.finnilius.api.Main;

public class MySQLBusiness {

    private static int lastBussinessID = 0;

    public static void loadData() {
        ResultSet businessData = Main.getFactory().query("SELECT * FROM Business");
        ResultSet businessPlayerData = Main.getFactory().query("SELECT * FROM BusinessPlayers");
        ResultSet businessBankData = Main.getFactory().query("SELECT * FROM BusinessBank");
        ResultSet businessBuildData = Main.getFactory().query("SELECT * FROM BusinessBuild");
        ResultSet businessShopData = Main.getFactory().query("SELECT * FROM BusinessShops");
        try {
            while (businessData.next()) {
                Main.getDbDataProvider().getBusinesses().add(new Business(
                        businessData.getInt("ID"),
                        businessData.getString("Name"),
                        businessData.getString("UUID"),
                        businessData.getString("Date"),
                        businessData.getString("Description"),
                        businessData.getString("License"),
                        businessData.getInt("Level"),
                        businessData.getDouble("Money")
                ));
                lastBussinessID = businessData.getInt("ID");
            }
            while (businessPlayerData.next()) {
                Main.getDbDataProvider().getBusinessPlayers().add(new BusinessPlayers(
                        businessPlayerData.getString("UUID"),
                        businessPlayerData.getInt("ID"),
                        businessPlayerData.getString("Name"),
                        businessPlayerData.getString("Rank"),
                        businessPlayerData.getDouble("Money")
                ));
            }
            while (businessBankData.next()) {
                Main.getDbDataProvider().getBusinessBanks().add(new BusinessBank(
                        businessBankData.getInt("ID"),
                        businessBankData.getString("Name"),
                        businessBankData.getString("FromUUID"),
                        businessBankData.getString("ToUUID"),
                        businessBankData.getString("Status"),
                        businessBankData.getString("Description"),
                        businessBankData.getString("License"),
                        businessBankData.getInt("Level"),
                        businessBankData.getDouble("Money"),
                        businessBankData.getDouble("CEO"),
                        businessBankData.getDouble("Employee")
                ));
            }
            while (businessBuildData.next()) {
                Main.getDbDataProvider().getBusinessBuilds().add(new BusinessBuild(
                        businessBuildData.getInt("ID"),
                        businessBuildData.getString("Name"),
                        businessBuildData.getString("FromUUID"),
                        businessBuildData.getString("ToUUID"),
                        businessBuildData.getString("Status"),
                        businessBuildData.getString("Description"),
                        businessBuildData.getString("License"),
                        businessBuildData.getInt("Level"),
                        businessBuildData.getDouble("Money"),
                        businessBuildData.getDouble("CEO"),
                        businessBuildData.getDouble("Employee")
                ));
            }
            while (businessShopData.next()) {
                Main.getDbDataProvider().getBusinessShops().add(new BusinessShops(
                        businessShopData.getInt("ID"),
                        businessShopData.getInt("BID"),
                        businessShopData.getString("Name"),
                        businessShopData.getString("Location"),
                        businessShopData.getString("Date"),
                        businessShopData.getString("Item"),
                        businessShopData.getInt("Sold"),
                        businessShopData.getString("Available")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBusinesses().size() + " Business geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBusinessPlayers().size() + " Business-Spieler geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBusinessBanks().size() + " Business-Banks geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBusinessBuilds().size() + " Business-Builds geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBusinessShops().size() + " Business-Shops geladen.");
    }

    public static Business getBusinessByID(int id) {
        for (Business business : Main.getDbDataProvider().getBusinesses()) {
            if (business.getId() == id) {
                return business;
            }
        }
        return null;
    }

    public static boolean isValidBusinessID(int id) {
        return getBusinessByID(id) != null;
    }

    public static BusinessPlayers getBusinessPlayerByUUID(String uuid) {
        for (BusinessPlayers businessPlayers : Main.getDbDataProvider().getBusinessPlayers()) {
            if (businessPlayers.getUuid().equals(uuid)) {
                return businessPlayers;
            }
        }
        return null;
    }

    public static boolean isValidBusinessPlayer(String uuid) {
        return getBusinessPlayerByUUID(uuid) != null;
    }

    public static BusinessShops getBusinessShopByID(int id) {
        for (BusinessShops businessShops : Main.getDbDataProvider().getBusinessShops()) {
            if (businessShops.getId() == id) {
                return businessShops;
            }
        }
        return null;
    }

    public static boolean isValidBusinessShop(int id) {
        return getBusinessShopByID(id) != null;
    }

    //Erstellt eine Firma
    public static void createBusiness(String businessName, String playerUUID, String businessLicense, String foundingDate) {
        lastBussinessID++;
        Main.getDbDataProvider().getBusinesses().add(new Business(lastBussinessID, businessName, playerUUID, foundingDate, "", businessLicense, 1, 0));
        Main.getFactory().update("INSERT INTO Business(Name, UUID, Date, Description, License, Level, Money) " + "VALUES ('" + businessName + "', '" + playerUUID + "', '" + foundingDate + "', '" + "" + "', '" + businessLicense + "', '1', '0');");
    }

    //Erstellt ein Mitglied der Firma
    public static void createMember(String playerUUID, String businessName, Integer businessID, String playerRank) {
        Main.getDbDataProvider().getBusinessPlayers().add(new BusinessPlayers(playerUUID, businessID, businessName, playerRank, 0));
        Main.getFactory().update("INSERT INTO BusinessPlayers(UUID, ID, Name, Rank, Money)" + "VALUES ('" + playerUUID + "', '" + businessID + "', '" + businessName + "', '" + playerRank + "', '0');");
    }

    //L�scht ein Mitglied
    public static void deleteMember(String playerUUID) {
        Main.getDbDataProvider().getBusinessPlayers().remove(getBusinessPlayerByUUID(playerUUID));
        Main.getFactory().update("DELETE FROM BusinessPlayers WHERE UUID = '" + playerUUID + "'");
    }

    //L�scht eine Firma
    public static void deleteBusiness(Integer businessID) {
        Main.getDbDataProvider().getBusinesses().remove(getBusinessByID(businessID));
        Main.getFactory().update("DELETE FROM Business WHERE ID = '" + businessID + "'");
    }

    //Alle Firmen
    public static void getBusinessList() {

    }

    public static void getBankNumber(Integer businessID) {

    }

    //Liest den Namen der Firma mit der ID aus
    public static String getBusinessName(Integer businessID) {
        if (!isValidBusinessID(businessID)) return null;
        return getBusinessByID(businessID).getName();
    }

    //�ndert den Namen
    public static void setName(Integer businessID, String businessName) {
        if (!isValidBusinessID(businessID)) return;
        getBusinessByID(businessID).setName(businessName);
        for (BusinessPlayers businessPlayers : Main.getDbDataProvider().getBusinessPlayers()) {
            if (businessPlayers.getId() == businessID) {
                businessPlayers.setName(businessName);
            }
        }
        for (BusinessShops shops : Main.getDbDataProvider().getBusinessShops()) {
            if (shops.getBid() == businessID) {
                shops.setName(businessName);
            }
        }
        /*
        for (BusinessBuild build : Main.getDbDataProvider().getBusinessBuilds()){

        }
        */
        /*
        for (BusinessBank bank : Main.getDbDataProvider().getBusinessBanks()){

        }
        */
        Main.getFactory().update("UPDATE Business SET Name = '" + businessName + "' WHERE ID = '" + businessID + "'");
        Main.getFactory().update("UPDATE BusinessPlayers SET Name = '" + businessName + "' WHERE BID = '" + businessID + "'");
        Main.getFactory().update("UPDATE BusinessShops SET Name = '" + businessName + "' WHERE BID = '" + businessID + "'");
        Main.getFactory().update("UPDATE BusinessBuild SET Name = '" + businessName + "' WHERE BID = '" + businessID + "'");
        Main.getFactory().update("UPDATE BusinessBank SET Name = '" + businessName + "' WHERE BID = '" + businessID + "'");
    }

    //Liest die ID der Firma mit einem Spieler aus
    public static int getBusinessIDWithPlayer(String playerUUID) {
        if (!isValidBusinessPlayer(playerUUID)) return -1;
        return getBusinessPlayerByUUID(playerUUID).getId();
    }

    //Liest die ID mit dem Namen aus
    public static int getBusinessIDWithName(String businessName) {
        for (Business business : Main.getDbDataProvider().getBusinesses()) {
            if (business.getName().equals(businessName)) {
                return business.getId();
            }
        }
        return -1;
    }

    //Liest die Mitarbeiter aus
    public static ArrayList<String> getMember(Integer businessID) {
        if (!isValidBusinessID(businessID)) return null;
        ArrayList<String> member = new ArrayList<>();
        for (BusinessPlayers players : Main.getDbDataProvider().getBusinessPlayers()) {
            if (players.getId() == businessID) {
                member.add(players.getName());
            }
        }
        return member;
    }

    //Liest die Lizenzen aus
    public static String getLicense(Integer businessID) {
        if (!isValidBusinessID(businessID)) return null;
        return getBusinessByID(businessID).getLicense();
    }

    //Setzt die Lizenzen
    public static void setLicense(Integer businessID, String businessLicense) {
        getBusinessByID(businessID).setLicense(businessLicense);
        Main.getFactory().update("UPDATE Business SET License = '" + businessLicense + "' WHERE ID = '" + businessID + "'");
    }

    //Pr�ft ob der Spieler in einer Firma ist
    public static boolean hasBusiness(String playerUUID) {
        if (getBusinessIDWithPlayer(playerUUID) == -1) {
            return false;
        }
        return true;
    }

    //Checkt ob ein Spieler in der Datenbank ist
    public static boolean playerExists(String playerUUID) {
        return isValidBusinessPlayer(playerUUID);
    }

    //Checkt ob eine Firma in der Datenbank ist
    public static boolean businessExists(String businessName) {
        return getBusinessIDWithName(businessName) == -1;
    }

    //Setzt Geld
    public static void setMoney(Integer businessID, double money) {
        getBusinessByID(businessID).setMoney(money);
        Main.getFactory().update("UPDATE Business SET Money = '" + money + "' WHERE ID = '" + businessID + "'");
    }

    //F�gt Geld hinzu
    public static void addMoney(Integer businessID, double money) {
        setMoney(businessID, getMoney(businessID) + money);
    }

    //Entfernt Geld
    public static void removeMoney(Integer businessID, double money) {
        setMoney(businessID, getMoney(businessID) - money);
    }

    //Liest das Geld aus
    public static double getMoney(Integer businessID) {
        if (!isValidBusinessID(businessID)) return -1;
        return getBusinessByID(businessID).getMoney();
    }

    //Setzt ein Level
    public static void setLevel(Integer businessID, int businessLevel) {
        getBusinessByID(businessID).setLevel(businessLevel);
        Main.getFactory().update("UPDATE Business SET Level = '" + businessLevel + "' WHERE ID = '" + businessID + "'");
    }

    //F�gt ein Level hinzu
    public static void addLevel(Integer businessID, int businessLevel) {
        setLevel(businessID, getLevel(businessID) + businessLevel);
    }

    //Liest das Level aus
    public static int getLevel(Integer businessID) {
        if (!isValidBusinessID(businessID)) return -1;
        return getBusinessByID(businessID).getLevel();
    }

    //Liest die UUID des Besitzers aus
    public static String getOwnerUUID(Integer businessID) {
        if (!isValidBusinessID(businessID)) return null;
        return getBusinessByID(businessID).getUuid();
    }

    //Setzt einen Besitzer
    public static void setOwner(Integer businessID, String playerUUID) {
        getBusinessByID(businessID).setUuid(playerUUID);
        Main.getFactory().update("UPDATE Business SET UUID = '" + playerUUID + "' WHERE ID = '" + businessID + "'");
    }

    //Setzt eine Beschreibung
    public static void setDescription(Integer businessID, String businessDescription) {
        getBusinessByID(businessID).setDescription(businessDescription);
        Main.getFactory().update("UPDATE Business SET Description = '" + businessDescription + "' WHERE ID = '" + businessID + "'");
    }

    //Liest eine Beschreibung aus
    public static String getDescription(Integer businessID) {
        if (!isValidBusinessID(businessID)) return null;
        return getBusinessByID(businessID).getDescription();
    }

    //Liest das Gr�ndungsdatum aus
    public static String getDate(Integer businessID) {
        if (!isValidBusinessID(businessID)) return null;
        return getBusinessByID(businessID).getDate();
    }

    public static String getRank(String playerUUID) {
        if (!isValidBusinessPlayer(playerUUID)) return null;
        return getBusinessPlayerByUUID(playerUUID).getRank();
    }

    public static void setRank(String playerUUID, String businessRank) {
        getBusinessPlayerByUUID(playerUUID).setRank(businessRank);
        Main.getFactory().update("UPDATE BusinessPlayers SET Rank = '" + businessRank + "' WHERE UUID = '" + playerUUID + "'");
    }

    public static double getSalary(String playerUUID) {
        if (!isValidBusinessPlayer(playerUUID)) return -1;
        return getBusinessPlayerByUUID(playerUUID).getMoney();
    }

    public static void setSalary(String playerUUID, double salary) {
        getBusinessPlayerByUUID(playerUUID).setMoney(salary);
        Main.getFactory().update("UPDATE BusinessPlayers SET Money = '" + salary + "' WHERE UUID = '" + playerUUID + "'");
    }

    //Erstellt einen neuen Chestshop
    public static void createShop(Integer businessID, String businessName, Location location, String date, String itemType) {
        String locationAsString = location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getBlockZ() + ";" + location.getYaw() + ";" + location.getPitch() + ";";
        Main.getDbDataProvider().getBusinessShops().add(new BusinessShops(lastBussinessID, businessID, businessName, locationAsString, date, itemType, 0, "" + true));
        Main.getFactory().update("INSERT INTO BusinessShops(BID, Name, Location, Date, Item, Sold, Available) " + "VALUES ('" + businessID + "', '" + businessName + "', '" + locationAsString + "','" + date + "', '" + itemType + "', '0', '" + "TRUE" + "');");
    }

    //L�scht einen Chestshop
    public static void deleteShop(Integer shopID) {
        Main.getDbDataProvider().getBusinessShops().remove(getBusinessShopByID(shopID));
        Main.getFactory().update("DELETE FROM BusinessShops WHERE ID = '" + shopID + "'");
    }

    //Pr�ft, ob es sich um einen Chestshop handelt
    public static boolean isShop(Location location) {
        String locationAsString = location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getBlockZ() + ";" + location.getYaw() + ";" + location.getPitch() + ";";
        for (BusinessShops shops : Main.getDbDataProvider().getBusinessShops()) {
            if (shops.getLocation().equals(locationAsString)) {
                return true;
            }
        }
        return false;
    }

    //Pr�ft, ob es sich um einen Chestshop handelt
    public static boolean isAvailable(Integer shopID) {
        if (!isValidBusinessShop(shopID)) return false;
        return getBusinessShopByID(shopID).getAvailable() != "TRUE";
    }

    public static Integer getShopBusinessID(Integer shopID) {
        if (!isValidBusinessShop(shopID)) return -1;
        return getBusinessShopByID(shopID).getBid();
    }

    //Die Location des Chestshops
    public static Location getShopLocation(Integer chestshopID) {
        if (!isValidBusinessShop(chestshopID)) return null;
        String locationString = getBusinessShopByID(chestshopID).getLocation();
        String[] locationData = null;
        if (locationString == null) return null;
        locationData = locationString.split(";");
        String world = locationData[0];
        double x = Double.parseDouble(locationData[1]);
        double y = Double.parseDouble(locationData[2]);
        double z = Double.parseDouble(locationData[3]);

        float yaw = Float.parseFloat(locationData[4]);
        float pitch = Float.parseFloat(locationData[5]);

        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

    //Liest die ID des Chestshops mit der Location aus
    public static int getShopIDWithLocation(Location location) {
        String locationAsString = location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getBlockZ() + ";" + location.getYaw() + ";" + location.getPitch() + ";";
        for (BusinessShops shops : Main.getDbDataProvider().getBusinessShops()) {
            if (shops.getLocation().equals(locationAsString)) {
                return shops.getId();
            }
        }
        return -1;
    }

    //Liest alle Chestshops der Firma aus
    public static ArrayList<Integer> getShops(Integer businessID) {
        ArrayList<Integer> shopList = new ArrayList<>();
        for (BusinessShops shops : Main.getDbDataProvider().getBusinessShops()) {
            if (shops.getId() == businessID) {
                shopList.add(shops.getId());
            }
        }
        return shopList;
    }

    public static int getSold(Integer shopID) {
        if (!isValidBusinessShop(shopID)) return -1;
        return getBusinessShopByID(shopID).getSlotID();
    }

    public static void setSold(Integer shopID, int amount) {
        if(!isValidBusinessID(shopID)) return;
        getBusinessShopByID(shopID).setSlotID(amount);
        Main.getFactory().update("UPDATE BusinessShops SET Sold = '" + amount + "' WHERE ID = '" + shopID + "'");
    }

    public static String getItem(Integer shopID) {
        if (!isValidBusinessShop(shopID)) return null;
        return getBusinessShopByID(shopID).getItem();
    }

    public static String getShopDate(Integer shopID) {
        if (!isValidBusinessShop(shopID)) return null;
        return getBusinessShopByID(shopID).getDate();
    }
}
