package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import de.finnilius.api.dbmodels.Ban;
import de.finnilius.api.dbmodels.Mobile;
import org.bukkit.Bukkit;

import de.finnilius.api.Main;

public class MySQLMobile {

    public static void loadData() {
        ResultSet mobileData = Main.getFactory().query("SELECT * FROM Mobile");
        try {
            while (mobileData.next()) {
                Main.getDbDataProvider().getMobiles().add(new Mobile(
                        mobileData.getString("UUID"),
                        mobileData.getString("Number"),
                        mobileData.getString("Contacts"),
                        mobileData.getString("Apps")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getMobiles().size() + " Mobiles geladen.");
    }

    private static Mobile getMobileByUUID(String uuid) {
        for (Mobile mobile : Main.getDbDataProvider().getMobiles()) {
            if (mobile.getUuid().equals(uuid)) return mobile;
        }
        return null;
    }

    public static boolean playerExists(String playerUUID) {
        return getMobileByUUID(playerUUID) != null;
    }

    public static String createHandyNumber() {
        String number = "+49 ";
        Random value = new Random();
        int size = value.nextInt(3);

        int count = 0;
        int n = 0;
        for (int i = 0; i < 5 + size; i++) {
            if (count == 4) {
                number += "";
                count = 0;
            } else
                n = value.nextInt(10);
            number += Integer.toString(n);
            count++;
        }
        return number;
    }

    public static void createPlayer(String playerUUID) {
        String nummer = createHandyNumber();
        if (!playerExists(playerUUID)) {
            Main.getDbDataProvider().getMobiles().add(new Mobile(playerUUID, nummer, "", ""));
            Main.getFactory().update("INSERT INTO Mobile (UUID, Number, Contacts, Apps) VALUES ('" + playerUUID + "',  '" + nummer + "', '', '')");
        }
    }

    public static String getNumber(String playerUUID) {
        if (!playerExists(playerUUID)) return null;
        return getMobileByUUID(playerUUID).getNumber();
    }

    public static String getKontakte(String playerUUID) {
        if (!playerExists(playerUUID)) return null;
        return getMobileByUUID(playerUUID).getContacts();
    }

    public static String getApps(String playerUUID) {
        if (!playerExists(playerUUID)) return null;
        return getMobileByUUID(playerUUID).getApps();
    }

    public static void addKontakt(String playerUUID, String targetUUID) {
        if (!playerExists(playerUUID)) return;

        String kontakteAsList = getKontakte(playerUUID);
        kontakteAsList = kontakteAsList + targetUUID + ";";

        getMobileByUUID(playerUUID).setContacts(kontakteAsList);
        Main.getFactory().update("UPDATE Mobile SET Contacts = '" + kontakteAsList + "' WHERE UUID= '" + playerUUID + "'");
    }

    public static void removeKontakt(String playerUUID, String targetUUID) {
        if (!playerExists(playerUUID)) return;

        String kontakteAsList = getKontakte(playerUUID);
        kontakteAsList = kontakteAsList.replace(targetUUID + ";", "");

        getMobileByUUID(playerUUID).setContacts(kontakteAsList);
        Main.getFactory().update("UPDATE Mobile SET Contacts = '" + kontakteAsList + "' WHERE UUID= '" + playerUUID + "'");
    }

    public static void addApp(String playerUUID, Enum<?> newApp) {
        if (!playerExists(playerUUID)) return;

        String appsAsList = getApps(playerUUID);
        appsAsList = appsAsList + newApp + ";";

        getMobileByUUID(playerUUID).setApps(appsAsList);
        Main.getFactory().update("UPDATE Mobile SET Apps = '" + appsAsList + "' WHERE UUID= '" + playerUUID + "'");
    }

    public static void removeApp(String playerUUID, Enum<?> targetApp) {
        if (!playerExists(playerUUID)) return;

        String appsAsList = getApps(playerUUID);
        appsAsList = appsAsList.replace(targetApp + ";", "");

        getMobileByUUID(playerUUID).setApps(appsAsList);
        Main.getFactory().update("UPDATE Mobile SET Apps= '" + appsAsList + "' WHERE UUID= '" + playerUUID + "'");
    }
}
