package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

import de.finnilius.api.Main;
import de.finnilius.api.dbmodels.Ban;

public class MySQLBan { // BanSystem: UUID, Name, Ende, Von, Grund

    public static void loadData() {
        ResultSet banData = Main.getFactory().query("SELECT * FROM Ban");
        Main.getDbDataProvider().getBan().clear();
        try {
            while (banData.next()) {
                Main.getDbDataProvider().getBan().add(new Ban(
                        banData.getString("UUID"),
                        banData.getString("Time"),
                        banData.getString("Punisher"),
                        banData.getString("Reason")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBan().size() + " Bans geladen.");
    }

    public static Ban getBanFromUUID(String uuid) {
        for (Ban ban : Main.getDbDataProvider().getBan()) {
            if (ban.getUuid().equals(uuid)) {
                return ban;
            }
        }
        return null;
    }

    public static void ban(String uuid, long bis, String von, String grund) {
        if (!isBanned(uuid)) {
            long end = 0L;
            if (bis == -1L) {
                end = -1L;
            } else {
                long cuurent = System.currentTimeMillis();
                long millis = bis * 1000L;
                end = cuurent + millis;
            }
            Main.getDbDataProvider().getBan().add(new Ban(uuid, "" + bis, von, grund));
            Main.getFactory().update("INSERT INTO Ban (UUID, Time, Punisher, Reason) VALUES ('" + uuid + "', '" + end + "', '" + von + "', '" + grund + "')");
        }
    }

    public static void unban(String uuid) {
        if (isBanned(uuid)) {
            Main.getDbDataProvider().getBan().remove(getBanFromUUID(uuid));
            Main.getFactory().update("DELETE FROM Ban WHERE UUID='" + uuid + "'");
        }
    }

    public static boolean isBanned(String uuid) {
        return getBanFromUUID(uuid) != null;
    }

    public static String getReason(String uuid) {
        if(!isBanned(uuid)) return null;
        return getBanFromUUID(uuid).getReason();
    }

    public static String getBy(String uuid) {
        if(!isBanned(uuid)) return null;
        return getBanFromUUID(uuid).getPunisher();
    }

    public static Long getEnding(String uuid) {
        if(!isBanned(uuid)) return null;
        return Long.getLong(getBanFromUUID(uuid).getTime());
    }

    public static String convertEnde(Long ende) {
        if (ende == -1L) {
            return "Permanent";
        }
        Time rm = new Time(ende);
        return rm.toString();
    }
}
