package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.finnilius.api.dbmodels.GroupPlayers;
import de.finnilius.api.dbmodels.Groups;
import de.finnilius.api.dbmodels.Secrets;
import de.finnilius.api.dbmodels.SecretsPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import de.finnilius.api.Main;

public class MySQLSecrets {

    private static int lastSecretID = 0;

    public static void loadData() {
        ResultSet secretData = Main.getFactory().query("SELECT * FROM Secrets");
        ResultSet secretPlayerData = Main.getFactory().query("SELECT * FROM SecretsPlayer");
        try {
            while (secretData.next()) {
                Main.getDbDataProvider().getSecrets().add(new Secrets(
                        secretData.getInt("ID"),
                        secretData.getString("Name"),
                        secretData.getString("Region"),
                        secretData.getString("Location")
                ));
                lastSecretID = secretData.getInt("ID");
            }
            while (secretPlayerData.next()) {
                Main.getDbDataProvider().getSecretsPlayers().add(new SecretsPlayer(
                        secretPlayerData.getString("UUID"),
                        secretPlayerData.getString("IDs"),
                        secretPlayerData.getInt("Amount")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getSecrets().size() + " Secrets geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getSecretsPlayers().size() + " Secret-Spieler geladen.");
    }

    private static Secrets getSecretByLocation(String location) {
        for (Secrets secrets : Main.getDbDataProvider().getSecrets()) {
            if (secrets.getLocation().equalsIgnoreCase(location)) return secrets;
        }
        return null;
    }

    private static SecretsPlayer getSecretPlayerByUUID(String uuid) {
        for (SecretsPlayer secretsPlayer : Main.getDbDataProvider().getSecretsPlayers()) {
            if (secretsPlayer.getUuid().equals(uuid)) return secretsPlayer;
        }
        return null;
    }

    //Erstellt den Spieler in der Datenbank
    public static void createPlayer(String uuid, String name) {
        if (!playerExists(uuid)) {
            Main.getDbDataProvider().getSecretsPlayers().add(new SecretsPlayer(uuid, "", 0));
            Main.getFactory().update("INSERT INTO SecretsPlayer (UUID, IDs, Amount) VALUES ('" + uuid + "', '', '0')");
        }
    }

    //Pr�ft ob der Spieler bereits in der Datenbank ist
    public static boolean playerExists(String uuid) {
        return getSecretPlayerByUUID(uuid) != null;
    }

    public static boolean isSecret(String location) {
        return getSecretByLocation(location) != null;
    }

    //Erstellt ein Secrets
    public static void createSecret(String name, String region, Location location) {
        String locationAsString = location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getBlockZ() + ";" + location.getYaw() + ";" + location.getPitch() + ";";
        lastSecretID++;
        Main.getDbDataProvider().getSecrets().add(new Secrets(lastSecretID, name, region, locationAsString));
        Main.getFactory().update("INSERT INTO Secrets(Name, Region, Location) " + " VALUES ('" + name + "', '" + region + "', '" + locationAsString + "')");
    }

    //Addet dem Spieler ein Secrets
    public static void addSecret(String playerUUID, int secretID) {
        if (!playerExists(playerUUID)) return;

        String secretsAsList = getSecrets(playerUUID);
        secretsAsList = secretsAsList + secretID + ";";

        getSecretPlayerByUUID(playerUUID).setIds(secretsAsList);
        Main.getFactory().update("UPDATE SecretsPlayer SET IDs= '" + secretsAsList + "' WHERE UUID= '" + playerUUID + "'");
        setAmount(playerUUID, (getAmount(playerUUID) + 1));
    }

    //Setzt die Anzahl an Secerts
    public static void setAmount(String playerUUID, int amount) {
        if (!playerExists(playerUUID)) return;
        getSecretPlayerByUUID(playerUUID).setAmount(amount);
        Main.getFactory().update("UPDATE SecretsPlayer SET Amount = '" + amount + "' WHERE UUID = '" + playerUUID + "'");
    }

    //Die Anzahl an Secrets
    public static int getAmount(String playerUUID) {
        if (!playerExists(playerUUID)) return 0;
        return getSecretPlayerByUUID(playerUUID).getAmount();
    }

    //Secret ID mit der Location
    public static int getIDWithLocation(String location) {
        if (!isSecret(location)) return -1;
        return getSecretByLocation(location).getId();
    }

    public static Location getLocation(int id) {
        for (Secrets secrets : Main.getDbDataProvider().getSecrets()) {
            if (secrets.getId() == id) {
                String locationAsString = secrets.getLocation();
                String[] locationData = null;
                if (locationAsString == null) return null;
                locationData = locationAsString.split(";");

                String world = locationData[0];
                double x = Double.parseDouble(locationData[1]);
                double y = Double.parseDouble(locationData[2]);
                double z = Double.parseDouble(locationData[3]);

                float yaw = Float.parseFloat(locationData[4]);
                float pitch = Float.parseFloat(locationData[5]);

                return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
            }
        }
        return null;
    }


    public static String getSecrets(String playerUUID) {
        if (!playerExists(playerUUID)) return null;
        return getSecretPlayerByUUID(playerUUID).getIds();
    }
}

