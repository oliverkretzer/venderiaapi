package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.finnilius.api.Main;
import de.finnilius.api.dbmodels.Ban;
import de.finnilius.api.dbmodels.Messages;
import de.finnilius.api.methods.Message;

public class MySQLMessage { 

    public static void loadData(){
        ResultSet messagesData = Main.getFactory().query("SELECT * FROM Messages");
        try {
            while (messagesData.next()) {
                Main.getDbDataProvider().getMessages().add(new Messages(
                    messagesData.getString("Sender"),
                    messagesData.getString("Receiver"),
                    messagesData.getString("Date"),
                    messagesData.getString("Message")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getMessages().size() + " Messages geladen.");
    }

    public static void addMessage(String playerUUID, String targetUUID, String date, String message) {
        Main.getDbDataProvider().getMessages().add(new Messages(playerUUID, targetUUID, date, message));
        Main.getFactory().update("INSERT INTO Messages (Sender, Receiver, Date, Message) VALUES ('" + playerUUID + "', '" + targetUUID + "', '" + date + "', '" + message + "')");
    }

    public static Message[] getMessages(String playerUUID) {
        Message[] msgs = new Message[100];
        int count = 0;
        for (Messages messages : Main.getDbDataProvider().getMessages()){
            if(messages.getSender().equals(playerUUID)){
                if(count == 95) return msgs;
                msgs[count] = new Message(messages.getReceiver(), messages.getMessage(), messages.getDate());
                count++;
            }
        }
        return msgs;
    }

}