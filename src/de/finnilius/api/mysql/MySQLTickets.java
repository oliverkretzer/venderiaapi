package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.finnilius.api.Main;
import de.finnilius.api.dbmodels.Stats;
import de.finnilius.api.dbmodels.Tickets;

public class MySQLTickets {

    public static void loadData() {
        ResultSet ticketData = Main.getFactory().query("SELECT * FROM Tickets");
        try {
            while (ticketData.next()) {
                Main.getDbDataProvider().getTickets().add(new Tickets(
                        ticketData.getString("UUID"),
                        ticketData.getString("Type"),
                        ticketData.getString("Time")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getTickets().size() + " Tickets geladen.");
    }

    private static Tickets getTicketByUUID(String uuid) {
        for (Tickets tickets : Main.getDbDataProvider().getTickets()) {
            if (tickets.getUuid().equals(uuid)) return tickets;
        }
        return null;
    }

    //Pr�ft, ob der Spieler ein Ticket hat
    public static boolean hasTicket(String playerUUID) {
        return getTicketByUUID(playerUUID) != null;
    }

    //Addet ein Ticket
    public static void setTicket(String playerUUID, String typeOfTicket, long endOfTicket) {
        long end = 0L;
        long cuurent = System.currentTimeMillis();
        long millis = endOfTicket * 1000L;
        end = cuurent + millis;

        Main.getDbDataProvider().getTickets().add(new Tickets(playerUUID, typeOfTicket, "" + end));
        Main.getFactory().update("INSERT INTO Tickets (UUID, Type, Time) VALUES ('" + playerUUID + "', '" + typeOfTicket + "', '" + end + "')");
    }

    //L�scht ein Ticket
    public static void deleteTicket(String playerUUID) {
        Main.getDbDataProvider().getTickets().remove(getTicketByUUID(playerUUID));
        Main.getFactory().update("DELETE FROM Tickets WHERE UUID='" + playerUUID + "'");
    }

    //Der Zeitraum
    public static long getTime(String playerUUID) {
        if (!hasTicket(playerUUID)) return -1;
        return Long.parseLong(getTicketByUUID(playerUUID).getTime());
    }
}
