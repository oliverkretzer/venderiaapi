package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import de.finnilius.api.dbmodels.Ban;
import de.finnilius.api.dbmodels.PayDay;
import org.bukkit.Bukkit;

import de.finnilius.api.Main;

public class MySQLPayDay {

    public static void loadData() {
        ResultSet paydayData = Main.getFactory().query("SELECT * FROM PayDay");
        try {
            while (paydayData.next()) {
                Main.getDbDataProvider().getPayDays().add(new PayDay(
                        paydayData.getString("UUID"),
                        paydayData.getInt("Time"),
                        paydayData.getDouble("Trader"),
                        paydayData.getDouble("Jobs")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getPayDays().size() + " PayDays geladen.");
    }

    private static PayDay getPayDayByUUID(String uuid) {
        for (PayDay payDay : Main.getDbDataProvider().getPayDays()) {
            if (payDay.getUuid().equals(uuid)) return payDay;
        }
        return null;
    }

    public static boolean playerExists(String playerUUID) {
        return getPayDayByUUID(playerUUID) != null;
    }

    public static void createPlayer(String playerUUID) {
        if (!playerExists(playerUUID)) {
            Main.getDbDataProvider().getPayDays().add(new PayDay(playerUUID, 0, 0, 0));
            Main.getFactory().update("INSERT INTO PayDay (UUID, Time, Trader, Jobs) VALUES ('" + playerUUID + "', '0', '0', '0')");
        }

    }

    public static void addTime(String playerUUID) {
        if (playerExists(playerUUID)) {
            setTime(playerUUID, getTime(playerUUID) + 1);
        }
    }

    public static void setTime(String playerUUID, int playtime) {
        if (playerExists(playerUUID)) {
            getPayDayByUUID(playerUUID).setTime(playtime);
            Main.getFactory().update("UPDATE PayDay SET Time = '" + playtime + "' WHERE UUID = '" + playerUUID + "'");
        }
    }

    public static int getTime(String playerUUID) {
        if (!playerExists(playerUUID)) return 0;
        return getPayDayByUUID(playerUUID).getTime();
    }


    public static void setTraderMoney(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            getPayDayByUUID(playerUUID).setTrader(amount);
            Main.getFactory().update("UPDATE PayDay SET Trader='" + amount + "' WHERE UUID='" + playerUUID + "'");
        }
    }

    public static void setJobMoney(String playerUUID, double amount) {
        if (playerExists(playerUUID)) {
            getPayDayByUUID(playerUUID).setJobs(amount);
            Main.getFactory().update("UPDATE PayDay SET Jobs='" + amount + "' WHERE UUID='" + playerUUID + "'");
        }
    }

    public static Double getTraderMoney(String playerUUID) {
        if (!playerExists(playerUUID)) return 0.0;
        return getPayDayByUUID(playerUUID).getTrader();
    }

    public static Double getJobMoney(String playerUUID) {
        if (!playerExists(playerUUID)) return 0.0;
        return getPayDayByUUID(playerUUID).getJobs();
    }
}
