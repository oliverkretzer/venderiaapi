package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.finnilius.api.Main;
import de.finnilius.api.dbmodels.Ban;
import de.finnilius.api.dbmodels.Punishments;

public class MySQLPunishments {

    private static int lastID = 0;

    public static void loadData() {
        ResultSet punishmentData = Main.getFactory().query("SELECT * FROM Punishments");
        Main.getDbDataProvider().getPunishments().clear();
        try {
            while (punishmentData.next()) {
                Main.getDbDataProvider().getPunishments().add(new Punishments(
                        punishmentData.getInt("ID"),
                        punishmentData.getString("UUID"),
                        punishmentData.getString("Reason"),
                        punishmentData.getString("Punisher"),
                        punishmentData.getString("Date")
                ));
                lastID = punishmentData.getInt("ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getPunishments().size() + " Punishments geladen.");
    }

    public static Punishments getPunishmentByID(int id) {
        for (Punishments punishments : Main.getDbDataProvider().getPunishments()) {
            if (punishments.getId() == id) {
                return punishments;
            }
        }
        return null;
    }

    public static boolean isValidID(int id) {
        return getPunishmentByID(id) != null;
    }


    public static void punish(String uuid, String reason, String punisher, String date) {
        lastID++;
        Main.getDbDataProvider().getPunishments().add(new Punishments(lastID, uuid, reason, punisher, date));
        Main.getFactory().update("INSERT INTO Punishments (UUID, Reason, Punisher, Date) VALUES ('" + uuid + "', '" + reason + "', '" + punisher + "', '" + date + "')");
    }

    public static String getReason(int id) {
        if (isValidID(id)) return null;
        return getPunishmentByID(id).getReason();
    }

    public static String getDate(int id) {
        if (isValidID(id)) return null;
        return getPunishmentByID(id).getDate();
    }

    public static String getUUID(int id) {
        if (isValidID(id)) return null;
        return getPunishmentByID(id).getUuid();
    }

    public static ArrayList<Integer> getPunishments() {
        ArrayList<Integer> ids = new ArrayList<>();
        for (Punishments punishments : Main.getDbDataProvider().getPunishments()) {
            ids.add(punishments.getId());
        }
        return ids;
    }

    public static String getPunisher(int id) {
        if (isValidID(id)) return null;
        return getPunishmentByID(id).getPunisher();
    }
}
