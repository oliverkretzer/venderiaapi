package de.finnilius.api.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.finnilius.api.dbmodels.Balance;
import de.finnilius.api.dbmodels.BuyRegions;
import de.finnilius.api.dbmodels.Money;
import de.finnilius.api.dbmodels.RentRegions;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import de.finnilius.api.Main;

public class MySQLRegions {

    public static void loadData() {
        ResultSet rentData = Main.getFactory().query("SELECT * FROM RentRegions");
        ResultSet buyData = Main.getFactory().query("SELECT * FROM BuyRegions");
        try {
            while (rentData.next()) {
                Main.getDbDataProvider().getRentRegions().add(new RentRegions(
                        rentData.getString("Region"),
                        rentData.getDouble("Price"),
                        rentData.getString("Type"),
                        rentData.getString("State"),
                        rentData.getString("Location"),
                        rentData.getString("UUID"),
                        rentData.getLong("Duration")
                ));
            }
            while (buyData.next()) {
                Main.getDbDataProvider().getBuyRegions().add(new BuyRegions(
                        buyData.getString("Region"),
                        buyData.getDouble("Price"),
                        buyData.getString("Type"),
                        buyData.getString("State"),
                        buyData.getString("Location"),
                        buyData.getString("UUID"),
                        buyData.getLong("Resellprice")
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getRentRegions().size() + " RentRegions geladen.");
        System.out.println(Main.getPrefix() + "Es wurden " + Main.getDbDataProvider().getBuyRegions().size() + " BuyRegions geladen.");
    }

    private static RentRegions getRentRegionByRegion(String region) {
        for (RentRegions rentRegions : Main.getDbDataProvider().getRentRegions()) {
            if (rentRegions.getRegion().equalsIgnoreCase(region)) return rentRegions;
        }
        return null;
    }

    private static boolean isValidRegion(String table, String region) {
        return table.equalsIgnoreCase("RentRegions") ? getRentRegionByRegion(region) != null : getBuyRegionByRegion(region) != null;
    }

    private static BuyRegions getBuyRegionByRegion(String region) {
        for (BuyRegions buyRegions : Main.getDbDataProvider().getBuyRegions()) {
            if (buyRegions.getRegion().equalsIgnoreCase(region)) return buyRegions;
        }
        return null;
    }

    public static boolean isRegionAlreadyExist(String region, String table) {
        return isValidRegion(table, region);
    }

    public static ArrayList<String> getRegions(String table) {
        ArrayList<String> regions = new ArrayList<>();
        if (table.equalsIgnoreCase("RentRegions")) {
            for (RentRegions rentRegions : Main.getDbDataProvider().getRentRegions())
                regions.add(rentRegions.getRegion());
        } else {
            for (BuyRegions buyRegions : Main.getDbDataProvider().getBuyRegions())
                regions.add(buyRegions.getRegion());
        }
        return regions;
    }

    public static void createBuyRegion(String region, double regionPrice, String regionType, Location location) {
        String locationAsString = location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getBlockZ() + ";" + location.getYaw() + ";" + location.getPitch() + ";";
        Main.getDbDataProvider().getBuyRegions().add(new BuyRegions(region, regionPrice, regionType, "Zu verkaufen", locationAsString, "", 0));
        Main.getFactory().update("INSERT INTO BuyRegions(Region, Price, Type, State, Location, UUID, Resellprice) VALUES ('" + region + "', '" + regionPrice + "', '" + regionType + "', 'Zu verkaufen', '" + locationAsString + "' ,'', '" + "0" + "')");
    }

    public static void createRentRegion(String region, double regionPrice, String regionType, Location location) {
        String locationAsString = location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getBlockZ() + ";" + location.getYaw() + ";" + location.getPitch() + ";";
        Main.getDbDataProvider().getRentRegions().add(new RentRegions(region, regionPrice, regionType, "Zu vermieten", locationAsString, "", 0));
        Main.getFactory().update("INSERT INTO RentRegions(Region, Price, Type, State, Location, UUID, Duration) VALUES ('" + region + "', '" + regionPrice + "', '" + regionType + "', 'Zu vermieten', '" + locationAsString + "' ,'', '" + "0" + "')");
    }

    public static void deleteRegion(String region, String table) {
        if (table.equalsIgnoreCase("RentRegions")) {
            Main.getDbDataProvider().getRentRegions().remove(getRentRegionByRegion(region));
        } else {
            Main.getDbDataProvider().getBuyRegions().remove(getBuyRegionByRegion(region));
        }
        Main.getFactory().update("DELETE FROM " + table + " WHERE Region= '" + region + "'");
    }

    public static void setLocation(String region, String table, Location location) {
        if (!isValidRegion(table, region)) return;
        String locationAsString = location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getBlockZ() + ";" + location.getYaw() + ";" + location.getPitch() + ";";
        if (table.equalsIgnoreCase("RentRegions")) {
            getRentRegionByRegion(region).setLocation(locationAsString);
        } else {
            getBuyRegionByRegion(region).setLocation(locationAsString);
        }
        Main.getFactory().update("UPDATE " + table + " SET Location = '" + locationAsString + "' WHERE Region= '" + region + "'");
    }

    public static Location getLocation(String region, String table) {
        if(!isValidRegion(table, region)) return null;
        String locationAsString;
        if (table.equalsIgnoreCase("RentRegions")) {
            locationAsString = getRentRegionByRegion(region).getLocation();
        } else {
            locationAsString = getBuyRegionByRegion(region).getLocation();
        }
        String[] locationData = null;
        if (locationAsString == null) return null;
        locationData = locationAsString.split(";");

        String world = locationData[0];
        double x = Double.parseDouble(locationData[1]);
        double y = Double.parseDouble(locationData[2]);
        double z = Double.parseDouble(locationData[3]);

        float yaw = Float.parseFloat(locationData[4]);
        float pitch = Float.parseFloat(locationData[5]);

        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

    public static void setOwner(String region, String table, String name, String uuid) {
        if(!isValidRegion(table, region)) return;
        if (table.equalsIgnoreCase("RentRegions")) {
            getRentRegionByRegion(region).setUuid(uuid);
        } else {
            getBuyRegionByRegion(region).setUuid(uuid);
        }
        Main.getFactory().update("UPDATE " + table + " SET UUID= '" + uuid + "' WHERE Region= '" + region + "'");
    }

    public static void setPrice(String region, double price, String table) {
        if(!isValidRegion(table, region)) return;
        if (table.equalsIgnoreCase("RentRegions")) {
            getRentRegionByRegion(region).setPrice(price);
        } else {
            getBuyRegionByRegion(region).setPrice(price);
        }
        Main.getFactory().update("UPDATE " + table + " SET Price= '" + price + "' WHERE Region= '" + region + "'");
    }

    public static void setState(String region, String table, String state) {
        if(!isValidRegion(table, region)) return;
        if (table.equalsIgnoreCase("RentRegions")) {
            getRentRegionByRegion(region).setState(state);
        } else {
            getBuyRegionByRegion(region).setState(state);
        }
        Main.getFactory().update("UPDATE " + table + " SET State= '" + state + "' WHERE Region= '" + region + "'");
    }


    public static String getOwner(String region, String table) {
        if(!isValidRegion(table, region)) return null;
        if (table.equalsIgnoreCase("RentRegions")) {
            return getRentRegionByRegion(region).getUuid();
        } else {
            return getBuyRegionByRegion(region).getUuid();
        }
    }

    public static double getPrice(String region, String table) {
        if(!isValidRegion(table, region)) return -1;
        if (table.equalsIgnoreCase("RentRegions")) {
            return getRentRegionByRegion(region).getPrice();
        } else {
            return getBuyRegionByRegion(region).getPrice();
        }
    }

    public static String getState(String region, String table) {
        if(!isValidRegion(table, region)) return null;
        if (table.equalsIgnoreCase("RentRegions")) {
            return getRentRegionByRegion(region).getState();
        } else {
            return getBuyRegionByRegion(region).getState();
        }
    }

    public static String getType(String region, String table) {
        if(!isValidRegion(table, region)) return null;
        if (table.equalsIgnoreCase("RentRegions")) {
            return getRentRegionByRegion(region).getType();
        } else {
            return getBuyRegionByRegion(region).getType();
        }
    }

    public static void setResellprice(String region, double price) {
        if(!isValidRegion("BuyRegions", region)) return;
        getBuyRegionByRegion(region).setPrice(price);
        Main.getFactory().update("UPDATE BuyRegions SET Resellprice= '" + price + "' WHERE Region= '" + region + "'");
    }

    public static double getResellprice(String region) {
        if(!isValidRegion("BuyRegions", region)) return -1;
        return getBuyRegionByRegion(region).getResellPrice();
    }

    public static void setDuration(String region, long duration) {
        if(!isValidRegion("RentRegions", region)) return;
        getRentRegionByRegion(region).setDuration(duration);
        Main.getFactory().update("UPDATE RentRegions SET Duration= '" + duration + "' WHERE Region= '" + region + "'");
    }

    public static long getDuration(String region) {
        if(!isValidRegion("RentRegions", region)) return -1;
        return getRentRegionByRegion(region).getDuration();
    }
}
