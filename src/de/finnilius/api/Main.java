package de.finnilius.api;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;

import com.mysql.jdbc.PreparedStatement;
import de.finnilius.api.mysql.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin {

    private static Factory factory;
    private static DBDataProvider dbDataProvider;
    private static Main instance;

    @Override
    public void onEnable() {
        factory = new Factory();
        dbDataProvider = new DBDataProvider();
        factory.connect();
        instance = this;

        factory.createTables();
        
        MySQLBan.loadData();
        MySQLPunishments.loadData();
        MySQLBusiness.loadData();
        MySQLGroups.loadData();
        MySQLMailbox.loadData();
        MySQLMessage.loadData();
        MySQLMobile.loadData();
        MySQLMoney.loadData();
        MySQLSecrets.loadData();
        MySQLPayDay.loadData();
        MySQLStats.loadData();
        MySQLTickets.loadData();
    }

    @Override
    public void onDisable() {
        factory.disconnect();
    }

    public static Factory getFactory() {
        return factory;
    }

    public static DBDataProvider getDbDataProvider() {
        return dbDataProvider;
    }

    public static String getPrefix() {
        return "[VenderiaAPI] - ";
    }

    public static Main getInstance() {
        return instance;
    }
}